local K, C, L = unpack(select(2, ...))

local _G = _G
local print = print

local ReloadUI = _G.ReloadUI

-- GLOBALS: InstallStepComplete

local function SetupAddons()
	-- DBM
	if IsAddOnLoaded("DBM-Core") and IsAddOnLoaded("DBM-StatusBarTimers") then
		K.LoadDBMProfile()
	end

	-- !BugGrabber
	if IsAddOnLoaded("!BugGrabber") then
		K.LoadBugGrabberProfile()
	end

	-- BugSack
	if IsAddOnLoaded("BugSack") then
		K.LoadBugSackProfile()
	end

	-- Details
	if IsAddOnLoaded("Details") then
		K.LoadDetailsProfile()
	end

	-- MikScrollingBattleText
	if IsAddOnLoaded("MikScrollingBattleText") then
		K.LoadMSBTProfile()
	end

	-- Pawn
	if IsAddOnLoaded("Pawn") then
		K.LoadPawnProfile()
	end

	-- Recount
	if IsAddOnLoaded("Recount") then
		K.LoadRecountProfile()
	end

	-- Skada
	if IsAddOnLoaded("Skada") then
		K.LoadSkadaProfile()
	end

	-- BigWigs
	if IsAddOnLoaded("BigWigs") then
		K.LoadBigWigsProfile()
	end
end

function K.AddOnSettings(msg)
	if msg == "skada" then
		if IsAddOnLoaded("Skada") then
			K.LoadSkadaProfile(true)
			K.StaticPopup_Show("CHANGES_RL")
			print(L["AddOnData"].SkadaText)
		else
			print(L["AddOnData"].SkadaNotText)
		end
	elseif msg == "dbm" then
		if IsAddOnLoaded("DBM-Core") then
			K.LoadDBMProfile()
			K.StaticPopup_Show("CHANGES_RL")
			print(L["AddOnData"].DBMText)
		else
			print(L["AddOnData"].DBMNotText)
		end
	elseif msg == "bigwigs" then
		if IsAddOnLoaded("BigWigs") then
			K.LoadBigWigsProfile()
			K.StaticPopup_Show("CHANGES_RL")
			print(L["AddOnData"].BigWigsText)
		else
			print(L["AddOnData"].BigWigsNotText)
		end
	elseif msg == "pawn" then
		if IsAddOnLoaded("Pawn") then
			K.LoadPawnProfile()
			K.StaticPopup_Show("CHANGES_RL")
			print(L["AddOnData"].PawnText)
		else
			print(L["AddOnData"].PawnNotText)
		end
	elseif msg == "msbt" then
		if IsAddOnLoaded("MikScrollingBattleText") then
			K.LoadMSBTProfile()
			K.StaticPopup_Show("CHANGES_RL")
			print(L["AddOnData"].MSBTText)
		else
			print(L["AddOnData"].MSBTNotText)
		end
	elseif msg == "bugsack" then
		if IsAddOnLoaded("BugSack") then
			K.LoadBugSackProfile()
			K.StaticPopup_Show("CHANGES_RL")
			print(L["AddOnData"].BugSackText)
		else
			print(L["AddOnData"].BugSackNotText)
		end
	elseif msg == "buggrabber" then
		if IsAddOnLoaded("!BugGrabber") then
			K.LoadBugGrabberProfile()
			K.StaticPopup_Show("CHANGES_RL")
			print(L["AddOnData"].BugGrabberText)
		else
			print(L["AddOnData"].BugGrabberNotText)
		end
	elseif msg == "bt4" or msg == "bartender" then
		if IsAddOnLoaded("Bartender4") then
			K.LoadBartenderProfile()
			K.StaticPopup_Show("CHANGES_RL")
			print(L["AddOnData"].BartenderText)
		else
			print(L["AddOnData"].BartenderNotText)
		end
	elseif msg == "all" or msg == "addons" then
		SetupAddons()
		K.StaticPopup_Show("CHANGES_RL")
		K.Print(L["AddOnData"].AllAddOnsText)
	else
		print(L["AddOnData"].InfoText)
	end
end

K:RegisterChatCommand("settings", K.AddOnSettings)