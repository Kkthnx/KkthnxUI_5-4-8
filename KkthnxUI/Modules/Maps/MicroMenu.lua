local K, C = unpack(select(2, ...))
if C["Minimap"].Enable ~= true then
	return
end

local _G = _G

local ACHIEVEMENTS_GUILD_TAB = _G.ACHIEVEMENTS_GUILD_TAB
local ACHIEVEMENT_BUTTON = _G.ACHIEVEMENT_BUTTON
local BLIZZARD_STORE = _G.BLIZZARD_STORE
local CALENDAR_VIEW_EVENT = _G.CALENDAR_VIEW_EVENT
local CHARACTER_BUTTON = _G.CHARACTER_BUTTON
local CHAT_CHANNELS = _G.CHAT_CHANNELS
local COMMUNITIES = _G.COMMUNITIES
local COMPACT_UNIT_FRAME_PROFILE_AUTOACTIVATEPVE = _G.COMPACT_UNIT_FRAME_PROFILE_AUTOACTIVATEPVE
local COMPACT_UNIT_FRAME_PROFILE_AUTOACTIVATEPVP = _G.COMPACT_UNIT_FRAME_PROFILE_AUTOACTIVATEPVP
local C_Social_IsSocialEnabled = _G.C_Social_IsSocialEnabled
local CreateFrame = _G.CreateFrame
local ENCOUNTER_JOURNAL = _G.ENCOUNTER_JOURNAL
local EasyMenu = _G.EasyMenu
local GARRISON_TYPE_8_0_LANDING_PAGE_TITLE = _G.GARRISON_TYPE_8_0_LANDING_PAGE_TITLE
local GameMenuFrame = _G.GameMenuFrame
local HEIRLOOMS = _G.HEIRLOOMS
local HELP_BUTTON = _G.HELP_BUTTON
local HideUIPanel = _G.HideUIPanel
local IsAddOnLoaded = _G.IsAddOnLoaded
local IsInGuild = _G.IsInGuild
local IsShiftKeyDown = _G.IsShiftKeyDown
local LFG_TITLE = _G.LFG_TITLE
local LoadAddOn = _G.LoadAddOn
local MAINMENU_BUTTON = _G.MAINMENU_BUTTON
local MOUNTS = _G.MOUNTS
local MiniMapTrackingDropDown_Initialize = _G.MiniMapTrackingDropDown_Initialize
local PETS = _G.PETS
local PlaySound = _G.PlaySound
local QUESTLOG_BUTTON = _G.QUESTLOG_BUTTON
local RAID = _G.RAID
local SOCIAL_BUTTON = _G.SOCIAL_BUTTON
local SOCIAL_TWITTER_COMPOSE_NEW_TWEET = _G.SOCIAL_TWITTER_COMPOSE_NEW_TWEET
local SOCIAL_TWITTER_TWEET_NOT_LINKED = _G.SOCIAL_TWITTER_TWEET_NOT_LINKED
local SPELLBOOK_ABILITIES_BUTTON = _G.SPELLBOOK_ABILITIES_BUTTON
local ShowUIPanel = _G.ShowUIPanel
local TALENTS_BUTTON = _G.TALENTS_BUTTON
local TOY_BOX = _G.TOY_BOX
local UIDropDownMenu_Initialize = _G.UIDropDownMenu_Initialize
local UIParent = _G.UIParent
local WORLD_MAP = _G.WORLD_MAP

-- Create the new minimap tracking dropdown frame and initialize it
local KkthnxUIMiniMapTrackingDropDown = CreateFrame("Frame", "KkthnxUIMiniMapTrackingDropDown", UIParent, "UIDropDownMenuTemplate")
KkthnxUIMiniMapTrackingDropDown:SetID(1)
KkthnxUIMiniMapTrackingDropDown:SetClampedToScreen(true)
KkthnxUIMiniMapTrackingDropDown:Hide()
UIDropDownMenu_Initialize(KkthnxUIMiniMapTrackingDropDown, MiniMapTrackingDropDown_Initialize, "MENU")
KkthnxUIMiniMapTrackingDropDown.noResize = true

local menuFrame = CreateFrame("Frame", "MinimapRightClickMenu", UIParent, "UIDropDownMenuTemplate")
local guildText = IsInGuild() and ACHIEVEMENTS_GUILD_TAB or LOOKINGFORGUILD
local menuList = {
	{text = CHARACTER_BUTTON, notCheckable = 1, func = function()
		ToggleCharacter("PaperDollFrame")
	end},
	{text = SPELLBOOK_ABILITIES_BUTTON, notCheckable = 1, func = function()
		ToggleFrame(SpellBookFrame)
	end},
	{text = TALENTS_BUTTON, notCheckable = 1, func = function()
		if not PlayerTalentFrame then
			TalentFrame_LoadUI()
		end
		if not GlyphFrame then
			GlyphFrame_LoadUI()
		end
		if not PlayerTalentFrame:IsShown() then
			ShowUIPanel(PlayerTalentFrame)
		else
			HideUIPanel(PlayerTalentFrame)
		end
	end},
	{text = MOUNTS_AND_PETS, notCheckable = 1, func = function()
		TogglePetJournal()
	end},
	{text = ACHIEVEMENT_BUTTON, notCheckable = 1, func = function()
		ToggleAchievementFrame()
	end},
	{text = QUESTLOG_BUTTON, notCheckable = 1, func = function()
		ToggleFrame(QuestLogFrame)
	end},
	{text = SOCIAL_BUTTON, notCheckable = 1, func = function()
		ToggleFriendsFrame(1)
	end},
	{text = "Calendar", notCheckable = 1, func = function()
		if(not CalendarFrame) then
			LoadAddOn("Blizzard_Calendar")
		end
		Calendar_Toggle()
	end},
	--{text = "Farm Mode", notCheckable = 1, func = FarmMode},
	{text = BATTLEFIELD_MINIMAP, notCheckable = 1, func = function()
			ToggleBattlefieldMinimap()
	end},
	{text = TIMEMANAGER_TITLE, notCheckable = 1, func = function()
		ToggleTimeManager()
	end},
	{text = guildText, notCheckable = 1, func = function()
		ToggleGuildFrame()
	end},
	{text = PLAYER_V_PLAYER, notCheckable = 1, func = function()
		if UnitLevel("player") >= SHOW_PVP_LEVEL then
			TogglePVPUI()
		else
			UIErrorsFrame:AddMessage(format(FEATURE_BECOMES_AVAILABLE_AT_LEVEL, SHOW_PVP_LEVEL), 1, 0.1, 0.1)
		end
	end},
	{text = LFG_TITLE, notCheckable = 1, func = function()
		if UnitLevel("player") >= SHOW_LFD_LEVEL then
			ToggleLFDParentFrame()
		else
			UIErrorsFrame:AddMessage(format(FEATURE_BECOMES_AVAILABLE_AT_LEVEL, SHOW_LFD_LEVEL), 1, 0.1, 0.1)
		end
	end},
	{text = ENCOUNTER_JOURNAL, notCheckable = 1, func = function()
		if not IsAddOnLoaded("Blizzard_EncounterJournal") then EncounterJournal_LoadUI() end
		ToggleFrame(EncounterJournal)
	end},
	{text = HELP_BUTTON, notCheckable = 1, func = function()
		ToggleHelpFrame()
	end},
	{text = MAINMENU_BUTTON, notCheckable = 1, func = function()
		if not GameMenuFrame:IsShown() then
			if VideoOptionsFrame:IsShown() then
				VideoOptionsFrameCancel:Click()
			elseif AudioOptionsFrame:IsShown() then
				AudioOptionsFrameCancel:Click()
			elseif InterfaceOptionsFrame:IsShown() then
				InterfaceOptionsFrameCancel:Click()
			end
			CloseMenus()
			CloseAllWindows()
			PlaySound("igMainMenuOpen")
			ShowUIPanel(GameMenuFrame)
		else
			PlaySound("igMainMenuQuit")
			HideUIPanel(GameMenuFrame)
			MainMenuMicroButton_SetNormal()
		end
	end}
}

Minimap:SetScript("OnMouseUp", function(self, btn)
	HideDropDownMenu(1, nil, KkthnxUIMiniMapTrackingDropDown)
	menuFrame:Hide()

	local position = self:GetPoint()
	if btn == "MiddleButton" or (btn == "RightButton" and IsShiftKeyDown()) then
		if position:match("LEFT") then
			EasyMenu(menuList, menuFrame, "cursor")
		else
			EasyMenu(menuList, menuFrame, "cursor", -160, 0)
		end
	elseif btn == "RightButton" then
		ToggleDropDownMenu(1, nil, KkthnxUIMiniMapTrackingDropDown, "cursor")
	else
		Minimap_OnClick(self)
	end
end)