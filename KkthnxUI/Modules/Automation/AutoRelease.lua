local K, C, L = unpack(select(2, ...))
if C["Automation"].AutoRelease ~= true then
	return
end

local Module = K:NewModule("AutoRelease", "AceEvent-3.0")

local _G = _G

local IsInInstance = _G.IsInInstance

-- Auto release the spirit in battlegrounds
function Module:PLAYER_DEAD()
	local inInstance, instanceType = IsInInstance()
	if inInstance and (instanceType == "pvp") then
		local soulstone = GetSpellInfo(20707)
		if K.Class ~= "SHAMAN" and not (soulstone and UnitBuff("player", soulstone)) then
			RepopMe()
		end
	end
end

function Module:OnEnable()
	self:RegisterEvent("PLAYER_DEAD")
end
