local K = unpack(select(2, ...))
local Module = K:GetModule("Unitframes")

local _G = _G
local string_lower = string.lower

local GetSpellInfo = _G.GetSpellInfo
local IsPlayerSpell = _G.IsPlayerSpell
local UnitClass = _G.UnitClass

Module.DebuffsTracking = {}

local function Defaults(priorityOverride)
	return {["enable"] = true, ["priority"] = priorityOverride or 0, ["stackThreshold"] = 0}
end

local function SpellName(id)
	local name = GetSpellInfo(id)
	if not name then
		print("|cff3c9bedKkthnxUI:|r SpellID is not valid: " .. id .. ". Please check for an updated version, if none exists report to Kkthnx in Discord.")
		return "Impale"
	else
		return name
	end
end

Module.DebuffsTracking["RaidDebuffs"] = {
	["type"] = "Whitelist",
	["spells"] = {
		-- Mogu'shan Vaults
		-- The Stone Guard
		[116281] = Defaults(),	-- Cobalt Mine Blast
		-- Feng the Accursed
		[116784] = Defaults(),	-- Wildfire Spark
		[116417] = Defaults(),	-- Arcane Resonance
		[116942] = Defaults(),	-- Flaming Spear
		-- Gara'jal the Spiritbinder
		[116161] = Defaults(),	-- Crossed Over
		[122151] = Defaults(),	-- Voodoo Doll
		-- The Spirit Kings
		[117708] = Defaults(),	-- Maddening Shout
		[118303] = Defaults(),	-- Fixate
		[118048] = Defaults(),	-- Pillaged
		[118135] = Defaults(),	-- Pinned Down
		-- Elegon
		[117878] = Defaults(),	-- Overcharged
		[117949] = Defaults(),	-- Closed Circuit
		-- Will of the Emperor
		[116835] = Defaults(),	-- Devastating Arc
		[116778] = Defaults(),	-- Focused Defense
		[116525] = Defaults(),	-- Focused Assault
		-- Heart of Fear
		-- Imperial Vizier Zor'lok
		[122761] = Defaults(),	-- Exhale
		[122760] = Defaults(),	-- Exhale
		[122740] = Defaults(),	-- Convert
		[123812] = Defaults(),	-- Pheromones of Zeal
		-- Blade Lord Ta'yak
		[123180] = Defaults(),	-- Wind Step
		[123474] = Defaults(),	-- Overwhelming Assault
		-- Garalon
		[122835] = Defaults(),	-- Pheromones
		[123081] = Defaults(),	-- Pungency
		-- Wind Lord Mel'jarak
		[122125] = Defaults(),	-- Corrosive Resin Pool
		[121885] = Defaults(),	-- Amber Prison
		-- Wind Lord Mel'jarak
		[121949] = Defaults(),	-- Parasitic Growth
		-- Terrace of Endless Spring
		-- Protectors of the Endless
		[117436] = Defaults(),	-- Lightning Prison
		[118091] = Defaults(),	-- Defiled Ground
		[117519] = Defaults(),	-- Touch of Sha
		-- Tsulong
		[122752] = Defaults(),	-- Shadow Breath
		[123011] = Defaults(),	-- Terrorize
		[116161] = Defaults(),	-- Crossed Over
		-- Lei Shi
		[123121] = Defaults(),	-- Spray
		-- Sha of Fear
		[119985] = Defaults(),	-- Dread Spray
		[119086] = Defaults(),	-- Penetrating Bolt
		[119775] = Defaults(),	-- Reaching Attack
		[122151] = Defaults(),	-- Voodoo Doll
		-- Throne of Thunder
		-- Trash
		[138349] = Defaults(),	-- Static Wound
		[137371] = Defaults(),	-- Thundering Throw
		-- Horridon
		[136767] = Defaults(),	-- Triple Puncture
		-- Council of Elders
		[137641] = Defaults(),	-- Soul Fragment
		[137359] = Defaults(),	-- Shadowed Loa Spirit Fixate
		[137972] = Defaults(),	-- Twisted Fate
		[136903] = Defaults(),	-- Frigid Assault
		-- Tortos
		[136753] = Defaults(),	-- Slashing Talons
		[137633] = Defaults(),	-- Crystal Shell
		-- Megaera
		[137731] = Defaults(),	-- Ignite Flesh
		-- Durumu the Forgotten
		[133767] = Defaults(),	-- Serious Wound
		[133768] = Defaults(),	-- Arterial Cut
		-- Primordius
		[136050] = Defaults(),	-- Malformed Blood
		-- Dark Animus
		[138569] = Defaults(),	-- Explosive Slam
		-- Iron Qon
		[134691] = Defaults(),	-- Impale
		-- Twin Consorts
		[137440] = Defaults(),	-- Icy Shadows
		[137408] = Defaults(),	-- Fan of Flames
		[137360] = Defaults(),	-- Corrupted Healing
		-- Lei Shen
		[135000] = Defaults(),	-- Decapitate
		-- Siege of Orgrimmar
		-- Immerseus
		[143436] = Defaults(),	-- Corrosive Blast
		[143579] = Defaults(),	-- Sha Corruption(Heroic)
		-- Fallen Protectors
		[147383] = Defaults(),	-- Debilitation
		-- Norushen
		[146124] = Defaults(),	-- Self Doubt
		[144851] = Defaults(),	-- Test of Confidence
		-- Sha of Pride
		[144358] = Defaults(),	-- Wounded Pride
		[144774] = Defaults(),	-- Reaching Attacks
		[147207] = Defaults(),	-- Weakened Resolve(Heroic)
		-- Kor'kron Dark Shaman
		[144215] = Defaults(),	-- Froststorm Strike
		[143990] = Defaults(),	-- Foul Geyser
		[144330] = Defaults(),	-- Iron Prison(Heroic)
		-- General Nazgrim
		[143494] = Defaults(),	-- Sundering Blow
		-- Malkorok
		[142990] = Defaults(),	-- Fatal Strike
		[143919] = Defaults(),	-- Languish(Heroic)
		-- Thok the Bloodthirsty
		[143766] = Defaults(),	-- Panic
		[143773] = Defaults(),	-- Freezing Breath
		[146589] = Defaults(),	-- Skeleton Key
		[143777] = Defaults(),	-- Frozen Solid
		-- Siegecrafter Blackfuse
		[143385] = Defaults(),	-- Electrostatic Charge
		-- Paragons of the Klaxxi
		[143974] = Defaults(),	-- Shield Bash
		-- Garrosh Hellscream
		[145183] = Defaults()	-- Gripping Despair
	}
}

-- CC DEBUFFS (TRACKING LIST)
Module.DebuffsTracking["CCDebuffs"] = {
	["type"] = "Whitelist",
	["spells"] = {
		-- Death Knight
		[47476] = Defaults(),	-- Strangulate
		[49203] = Defaults(),	-- Hungering Cold
		[91800] = Defaults(),	-- Gnaw (Pet)
		[91807] = Defaults(),	-- Shambling Rush (Pet)
		[91797] = Defaults(),	-- Monstrous Blow (Pet)
		[108194] = Defaults(),	-- Asphyxiate
		[115001] = Defaults(),	-- Remorseless Winter
		-- Druid
		[33786] = Defaults(),	-- Cyclone
		[2637] = Defaults(),	-- Hibernate
		[339] = Defaults(),		-- Entangling Roots
		[78675] = Defaults(),	-- Solar Beam
		[22570] = Defaults(),	-- Maim
		[5211] = Defaults(),	-- Mighty Bash
		[9005] = Defaults(),	-- Pounce
		[102359] = Defaults(),	-- Mass Entanglement
		[99] = Defaults(),		-- Disorienting Roar
		[127797] = Defaults(),	-- Ursol's Vortex
		[45334] = Defaults(),	-- Immobilized
		[102795] = Defaults(),	-- Bear Hug
		[114238] = Defaults(),	-- Fae Silence
		[113004] = Defaults(),	-- Intimidating Roar (Warrior Symbiosis)
		-- Hunter
		[3355] = Defaults(),	-- Freezing Trap
		[1513] = Defaults(),	-- Scare Beast
		[19503] = Defaults(),	-- Scatter Shot
		[34490] = Defaults(),	-- Silencing Shot
		[24394] = Defaults(),	-- Intimidation
		[64803] = Defaults(),	-- Entrapment
		[19386] = Defaults(),	-- Wyvern Sting
		[117405] = Defaults(),	-- Binding Shot
		[128405] = Defaults(),	-- Narrow Escape
		[50519] = Defaults(),	-- Sonic Blast (Bat)
		[91644] = Defaults(),	-- Snatch (Bird of Prey)
		[90337] = Defaults(),	-- Bad Manner (Monkey)
		[54706] = Defaults(),	-- Venom Web Spray (Silithid)
		[4167] = Defaults(),	-- Web (Spider)
		[90327] = Defaults(),	-- Lock Jaw (Dog)
		[56626] = Defaults(),	-- Sting (Wasp)
		[50245] = Defaults(),	-- Pin (Crab)
		[50541] = Defaults(),	-- Clench (Scorpid)
		[96201] = Defaults(),	-- Web Wrap (Shale Spider)
		[96201] = Defaults(),	-- Lullaby (Crane)
		-- Mage
		[31661] = Defaults(),	-- Dragon's Breath
		[118] = Defaults(),		-- Polymorph
		[55021] = Defaults(),	-- Silenced - Improved Counterspell
		[122] = Defaults(),		-- Frost Nova
		[82691] = Defaults(),	-- Ring of Frost
		[118271] = Defaults(),	-- Combustion Impact
		[44572] = Defaults(),	-- Deep Freeze
		[33395] = Defaults(),	-- Freeze (Water Ele)
		[102051] = Defaults(),	-- Frostjaw
		-- Paladin
		[20066] = Defaults(),	-- Repentance
		[10326] = Defaults(),	-- Turn Evil
		[853] = Defaults(),		-- Hammer of Justice
		[105593] = Defaults(),	-- Fist of Justice
		[31935] = Defaults(),	-- Avenger's Shield
		[105421] = Defaults(),	-- Blinding Light
		-- Priest
		[605] = Defaults(),		-- Dominate Mind
		[64044] = Defaults(),	-- Psychic Horror
		--[64058] = Defaults(),	-- Psychic Horror (Disarm)
		[8122] = Defaults(),	-- Psychic Scream
		[9484] = Defaults(),	-- Shackle Undead
		[15487] = Defaults(),	-- Silence
		[114404] = Defaults(),	-- Void Tendrils
		[88625] = Defaults(),	-- Holy Word: Chastise
		[113792] = Defaults(),	-- Psychic Terror (Psyfiend)
		[87194] = Defaults(),	-- Sin and Punishment
		-- Rogue
		[2094] = Defaults(),	-- Blind
		[1776] = Defaults(),	-- Gouge
		[6770] = Defaults(),	-- Sap
		[1833] = Defaults(),	-- Cheap Shot
		[51722] = Defaults(),	-- Dismantle
		[1330] = Defaults(),	-- Garrote - Silence
		[408] = Defaults(),		-- Kidney Shot
		[88611] = Defaults(),	-- Smoke Bomb
		[115197] = Defaults(),	-- Partial Paralytic
		[113953] = Defaults(),	-- Paralysis
		-- Shaman
		[51514] = Defaults(),	-- Hex
		[64695] = Defaults(),	-- Earthgrab
		[63685] = Defaults(),	-- Freeze (Frozen Power)
		[76780] = Defaults(),	-- Bind Elemental
		[118905] = Defaults(),	-- Static Charge
		[118345] = Defaults(),	-- Pulverize (Earth Elemental)
		-- Warlock
		[710] = Defaults(),		-- Banish
		[6789] = Defaults(),	-- Mortal Coil
		[118699] = Defaults(),	-- Fear
		[5484] = Defaults(),	-- Howl of Terror
		[6358] = Defaults(),	-- Seduction
		[30283] = Defaults(),	-- Shadowfury
		[24259] = Defaults(),	-- Spell Lock (Felhunter)
		[115782] = Defaults(),	-- Optical Blast (Observer)
		[115268] = Defaults(),	-- Mesmerize (Shivarra)
		[118093] = Defaults(),	-- Disarm (Voidwalker)
		[89766] = Defaults(),	-- Axe Toss (Felguard)
		[137143] = Defaults(),	-- Blood Horror
		-- Warrior
		[20511] = Defaults(),	-- Intimidating Shout
		[7922] = Defaults(),	-- Charge Stun
		[676] = Defaults(),		-- Disarm
		[105771] = Defaults(),	-- Warbringer
		[107566] = Defaults(),	-- Staggering Shout
		[132168] = Defaults(),	-- Shockwave
		[107570] = Defaults(),	-- Storm Bolt
		[118895] = Defaults(),	-- Dragon Roar
		[18498] = Defaults(),	-- Gag Order
		-- Monk
		[116706] = Defaults(),	-- Disable
		[117368] = Defaults(),	-- Grapple Weapon
		[115078] = Defaults(),	-- Paralysis
		[122242] = Defaults(),	-- Clash
		[119392] = Defaults(),	-- Charging Ox Wave
		[119381] = Defaults(),	-- Leg Sweep
		[120086] = Defaults(),	-- Fists of Fury
		[116709] = Defaults(),	-- Spear Hand Strike
		[123407] = Defaults(),	-- Spinning Fire Blossom
		[140023] = Defaults(),	-- Ring of Peace
		-- Racial
		[25046] = Defaults(),	-- Arcane Torrent
		[20549] = Defaults(),	-- War Stomp
		[107079] = Defaults() 	-- Quaking Palm
	}
}

-- RAID BUFFS (SQUARED AURA TRACKING LIST)
Module.RaidBuffsTracking = {
	PRIEST = {
		{6788, "TOPRIGHT", {1, 0, 0}, true},				-- Weakened Soul
		{41635, "BOTTOMRIGHT", {0.2, 0.7, 0.2}},			-- Prayer of Mending
		{139, "BOTTOMLEFT", {0.4, 0.7, 0.2}},				-- Renew
		{17, "TOPLEFT", {0.81, 0.85, 0.1}, true},			-- Power Word: Shield
		{123258, "TOPLEFT", {0.81, 0.85, 0.1}, true},	-- Power Word: Shield Power Insight
		{10060 , "RIGHT", {0.89, 0.09, 0.05}},			-- Power Infusion
		{47788, "LEFT", {0.86, 0.45, 0}, true},			-- Guardian Spirit
		{33206, "LEFT", {0.89, 0.09, 0.05}, true},		-- Pain Suppression
	},
	DRUID = {
		{774, "TOPRIGHT", {0.8, 0.4, 0.8}},				-- Rejuvenation
		{8936, "BOTTOMLEFT", {0.2, 0.8, 0.2}},			-- Regrowth
		{33763, "TOPLEFT", {0.4, 0.8, 0.2}},				-- Lifebloom
		{48438, "BOTTOMRIGHT", {0.8, 0.4, 0}},			-- Wild Growth
	},
	PALADIN = {
		{53563, "TOPRIGHT", {0.7, 0.3, 0.7}},			-- Beacon of Light
		{1022, "BOTTOMRIGHT", {0.2, 0.2, 1}, true},		-- Hand of Protection
		{1044, "BOTTOMRIGHT", {0.89, 0.45, 0}, true},		-- Hand of Freedom
		{1038, "BOTTOMRIGHT", {0.93, 0.75, 0}, true},		-- Hand of Salvation
		{6940, "BOTTOMRIGHT", {0.89, 0.1, 0.1}, true},	-- Hand of Sacrifice
		{114039, "BOTTOMRIGHT", {0.64, 0.41, 0.71}},	-- Hand of Purity
		{20925, 'TOPLEFT', {0.93, 0.75, 0}},				-- Sacred Shield
		{114163, 'BOTTOMLEFT', {0.87, 0.7, 0.03}},		-- Eternal Flame
	},
	SHAMAN = {
		{61295, "TOPRIGHT", {0.7, 0.3, 0.7}},			-- Riptide
		{974, "BOTTOMLEFT", {0.2, 0.7, 0.2}, true},		-- Earth Shield
		{51945, "BOTTOMRIGHT", {0.7, 0.4, 0}},			-- Earthliving
	},
	MONK = {
		{119611, "TOPLEFT", {0.8, 0.4, 0.8}},			-- Renewing Mist
		{116849, "TOPRIGHT", {0.2, 0.8, 0.2}},			-- Life Cocoon
		{132120, "BOTTOMLEFT", {0.4, 0.8, 0.2}},		-- Enveloping Mist
		{124081, "BOTTOMRIGHT", {0.7, 0.4, 0}},			-- Zen Sphere
	},
	ROGUE = {
		{57934, "TOPRIGHT", {0.89, 0.09, 0.05}},			-- Tricks of the Trade
	},
	MAGE = {
		{111264, "TOPLEFT", {0.2, 0.2, 1}},				-- Ice Ward
	},
	WARRIOR = {
		{114030, "TOPLEFT", {0.2, 0.2, 1}},				-- Vigilance
		{3411, "TOPRIGHT", {0.89, 0.09, 0.05}},			-- Intervene
		{114029, "TOPRIGHT", {0.89, 0.09, 0.05}},		-- Safe Guard
	},
	DEATHKNIGHT = {
		{49016, "TOPRIGHT", {0.89, 0.09, 0.05}},			-- Unholy Frenzy
	},
	PET = {
		{19615, 'TOPLEFT', {0.89, 0.09, 0.05}, true},	-- Frenzy
		{136, 'TOPRIGHT', {0.2, 0.8, 0.2}, true}			-- Mend Pet
	}
}

-- Stuff we need to see.
Module.ImportantDebuffs = {
	[SpellName(25771)] = K.Class == "PALADIN", -- Forbearance
	[SpellName(6788)] = K.Class == "PRIEST" -- Weakened Soul
}

-- Filter this. Pointless to see.
Module.UnImportantBuffs = {
	[SpellName(117870)] = true, -- Touch of The Titans
	[SpellName(124273)] = true,	-- Stagger
	[SpellName(124274)] = true,	-- Stagger
	[SpellName(124275)] = true,	-- Stagger
	[SpellName(132365)] = true,	-- Vengeance
	[SpellName(15007)] = true,	-- Resurrection Sickness
	[SpellName(23445)] = true,	-- Evil Twin
	[SpellName(24755)] = true,	-- Tricked or Treated Debuff
	[SpellName(25163)] = true,	-- Oozeling Disgusting Aura
	[SpellName(25771)] = true,	-- Forbearance
	[SpellName(26013)] = true,	-- Deserter
	[SpellName(36032)] = true,	-- Arcane Blast
	[SpellName(36032)] = true,	-- Arcane Charge
	[SpellName(41425)] = true,	-- Hypothermia
	[SpellName(46221)] = true,	-- Animal Blood
	[SpellName(55711)] = true,	-- Weakened Heart
	[SpellName(57723)] = true,	-- Exhaustion
	[SpellName(57724)] = true,	-- Sated
	[SpellName(58539)] = true,	-- Watchers Corpse
	[SpellName(6788)] = true,	-- Weakended Soul
	[SpellName(71041)] = true,	-- Dungeon Deserter
	[SpellName(80354)] = true,	-- Timewarp Debuff
	[SpellName(8326)] = true,	-- Ghost
	[SpellName(8733)] = true,	-- Blessing of Blackfathom
	[SpellName(95223)] = true	-- Group Res Debuff
}

-- List of spells to display ticks
Module.ChannelTicks = {
	-- Warlock
	[SpellName(1120)] = 6,		-- Drain Soul
	[SpellName(689)] = 6,		-- Drain Life
	[SpellName(108371)] = 6,	-- Harvest Life
	[SpellName(5740)] = 4,		-- Rain of Fire
	[SpellName(755)] = 6,		-- Health Funnel
	[SpellName(103103)] = 4,	-- Malefic Grasp
	-- Druid
	[SpellName(44203)] = 4,		-- Tranquility
	[SpellName(16914)] = 10,	-- Hurricane
	-- Priest
	[SpellName(15407)] = 3,		-- Mind Flay
	[SpellName(129197)] = 3,	-- Mind Flay (Insanity)
	[SpellName(48045)] = 5,		-- Mind Sear
	[SpellName(47540)] = 2,		-- Penance
	[SpellName(64901)] = 4,		-- Hymn of Hope
	[SpellName(64843)] = 4,		-- Divine Hymn
	-- Mage
	[SpellName(5143)] = 5,		-- Arcane Missiles
	[SpellName(10)] = 8,		-- Blizzard
	[SpellName(12051)] = 4,		-- Evocation
	--Monk
	[SpellName(115175)] = 9,	-- Smoothing Mist
}

Module.ChannelTicksSize = {
	--Warlock
	[SpellName(1120)] = 2,		-- Drain Soul
	[SpellName(689)] = 1,		-- Drain Life
	[SpellName(108371)] = 1,	-- Harvest Life
	[SpellName(103103)] = 1,	-- Malefic Grasp
}

-- Spells Effected By Haste
Module.HastedChannelTicks = {
	[SpellName(64901)] = true, -- Hymn of Hope
	[SpellName(64843)] = true, -- Divine Hymn
}