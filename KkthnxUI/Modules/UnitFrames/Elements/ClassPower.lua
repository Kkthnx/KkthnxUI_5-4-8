local K, C = unpack(select(2, ...))
if C["Unitframe"].Enable ~= true then
	return
end

local Module = K:GetModule("Unitframes")

local _G = _G

local CreateFrame = _G.CreateFrame

local ClassPowerTexture = K.GetTexture(C["Unitframe"].Texture)

local classMaxResourceBar = {
	["DEATHKNIGHT"] = 6,
	["PALADIN"] = 5,
	["MONK"] = 5,
	["WARLOCK"] = 4,
	["PRIEST"] = 3,
	["MAGE"] = 4,
	["ROGUE"] = 5,
}

local function eclipseBarBuff(self, unit)
	if self.hasSolarEclipse then
		self.eBarBG:SetBackdropBorderColor(1, 1, .5, .7)
	elseif self.hasLunarEclipse then
		self.eBarBG:SetBackdropBorderColor(.2, .2, 1, .7)
	else
		self.eBarBG:SetBackdropBorderColor()
	end
end

function Module:ClassResources()
	local count
	if K.Class == "PRIEST" then
		count = 3
	elseif K.Class == "PALADIN" then
		local numMax = UnitPowerMax("player", SPELL_POWER_HOLY_POWER)
		count = numMax
	elseif K.Class == "MONK" then
		local numMax = UnitPowerMax("player", SPELL_POWER_CHI)
		count = numMax
	end

	local ci = CreateFrame("Frame", nil, self)
	ci:SetPoint("TOPLEFT", self, "BOTTOMLEFT", 0, -5)
	ci:SetSize(self.Health and self.Health:GetWidth() or 140, 16)

	for i = 1, 5 do
		ci[i] = CreateFrame("StatusBar", self:GetName().."_ClassBar"..i, self)
		ci[i]:SetSize((self.Health:GetWidth()/count) -5, 16)
		ci[i]:SetStatusBarTexture(ClassPowerTexture)
		ci[i]:SetFrameLevel(1)
		ci[i].SetVertexColor = ci[i].SetStatusBarColor

		local h = CreateFrame("Frame", nil, ci[i])
		h:SetFrameLevel(1)
		h:SetPoint("TOPLEFT")
		h:SetPoint("BOTTOMRIGHT")
		h:CreateBorder()

		if (i == 1) then
			ci[i]:SetPoint("TOPLEFT", self.Power, "BOTTOMLEFT", 0, -6)
		else
			ci[i]:SetPoint("TOPLEFT", ci[i-1], "TOPRIGHT", 6, 0)
		end
	end
	self.ClassIcons = ci
end

function Module:CreateEclipseBar()
	local eclipseBar = CreateFrame("Frame", nil, self)
	eclipseBar:SetPoint("TOPLEFT", self.Power, "BOTTOMLEFT", 0, -6)
	eclipseBar:SetSize(self.Health and self.Health:GetWidth() or 140, 16)
	eclipseBar:SetFrameLevel(4)

	local h = CreateFrame("Frame", nil, eclipseBar)
	h:SetPoint("TOPLEFT")
	h:SetPoint("BOTTOMRIGHT")
	h:CreateBorder()
	eclipseBar.eBarBG = h

	local lunarBar = CreateFrame("StatusBar", nil, eclipseBar)
	lunarBar:SetPoint("LEFT", eclipseBar, "LEFT", 0, 0)
	lunarBar:SetSize(eclipseBar:GetWidth(), eclipseBar:GetHeight())
	lunarBar:SetStatusBarTexture(ClassPowerTexture)
	lunarBar:SetStatusBarColor(0, .1, .7)
	lunarBar:SetFrameLevel(5)

	local solarBar = CreateFrame("StatusBar", nil, eclipseBar)
	solarBar:SetPoint("LEFT", lunarBar:GetStatusBarTexture(), "RIGHT", 0, 0)
	solarBar:SetSize(eclipseBar:GetWidth(), eclipseBar:GetHeight())
	solarBar:SetStatusBarTexture(ClassPowerTexture)
	solarBar:SetStatusBarColor(1, 1,.13)
	solarBar:SetFrameLevel(5)

	local EBText = solarBar:CreateFontString(nil, "OVERLAY")
	EBText:SetFont(C.Media.Font, 10, "")
	EBText:SetShadowOffset(1, -1)
	EBText:SetPoint("CENTER", eclipseBar, "CENTER", 0, 0)
	self:Tag(EBText, "[pereclipse]")

	eclipseBar.SolarBar = solarBar
	eclipseBar.LunarBar = lunarBar
	self.EclipseBar = eclipseBar
	self.EclipseBar.PostUnitAura = eclipseBarBuff
end

function Module:CreateArcaneBar()
	local mb = CreateFrame("Frame", "ArcaneBar", self)
	mb:SetAllPoints(self.Health)

	for i = 1, 4 do
		mb[i] = CreateFrame("StatusBar", "ArcaneBar"..i, self)
		mb[i]:SetSize((self.Health:GetWidth()/4) -4.5, 16)
		mb[i]:SetFrameLevel(5)
		mb[i]:SetStatusBarTexture(ClassPowerTexture)
		mb[i]:SetStatusBarColor(104/255, 205/255, 255/255)

		if i == 1 then
			mb[i]:SetPoint("TOPLEFT", self.Power, "BOTTOMLEFT", 0, -6)
		else
			mb[i]:SetPoint("TOPLEFT", mb[i-1], "TOPRIGHT", 6, 0)
		end
		mb[i].bg = mb[i]:CreateTexture(nil, 'ARTWORK')

		local h = CreateFrame("Frame", nil, mb[i])
		h:SetFrameLevel(5)
		h:SetPoint("TOPLEFT")
		h:SetPoint("BOTTOMRIGHT")
		h:CreateBorder()
	end

	self.ArcaneChargeBar = mb
end

function Module:CreateComboBar()
	local barFrame = CreateFrame("Frame", nil, self)
	barFrame:SetPoint("TOPLEFT", self, "BOTTOMLEFT", 0, -5)
	barFrame:SetSize(self.Health and self.Health:GetWidth() or 140, 16)
	barFrame:SetFrameLevel(4)

	for i = 1, MAX_COMBO_POINTS do
		local point = CreateFrame("StatusBar", nil, barFrame)
		point:SetSize((self.Health:GetWidth() / 5) -5, 16)
		if i > 1 then
			point:SetPoint("LEFT", point[i - 1], "RIGHT")
		end
		point:SetStatusBarTexture(ClassPowerTexture)
		point:SetStatusBarColor(unpack(K.Colors.power.COMBO_POINTS[i]))

		local h = CreateFrame("Frame", nil, point)
		h:SetFrameLevel(5)
		h:SetPoint("TOPLEFT")
		h:SetPoint("BOTTOMRIGHT")
		h:CreateBorder()

		if (i == 1) then
			point:SetPoint("TOPLEFT", self.Power, "BOTTOMLEFT", 0, -6)
		else
			point:SetPoint("TOPLEFT", barFrame[i-1], "TOPRIGHT", 6, 0)
		end
		barFrame[i] = point
	end

	self.ComboPoints = barFrame
end

function Module:CreateRuneBar()
	local runeFrame = CreateFrame("Frame", nil, self)
	runeFrame:SetPoint("TOPLEFT", self, "BOTTOMLEFT", 0, -5)
	runeFrame:SetSize(self.Health and self.Health:GetWidth() or 140, 16)
	runeFrame:SetFrameLevel(4)

	for i = 1, 6 do
		local rune = CreateFrame("StatusBar", nil, runeFrame)
		rune:SetSize((self.Health:GetWidth() / 6) -5, 16)
		rune:SetStatusBarTexture(ClassPowerTexture)
		rune:SetFrameLevel(4)

		local h = CreateFrame("Frame", nil, rune)
		h:SetFrameLevel(4)
		h:SetPoint("TOPLEFT")
		h:SetPoint("BOTTOMRIGHT")
		h:CreateBorder()

		if (i == 1) then
			rune:SetPoint("TOPLEFT", self.Power, "BOTTOMLEFT", 0, -6)
		else
			rune:SetPoint("TOPLEFT", runeFrame[i-1], "TOPRIGHT", 6, 0)
		end

		runeFrame[i] = rune
	end

	self.Runes = runeFrame
end

function Module:CreateShardBar()
	local mb = CreateFrame("Frame", "ShardBar", self)
	mb:SetAllPoints(self.Health)

	for i = 1, 4 do
		mb[i] = CreateFrame("StatusBar", "ShardBar"..i, self)
		mb[i]:SetSize((self.Health:GetWidth() / 4) -4.5, 16)
		mb[i]:SetFrameLevel(5)
		mb[i]:SetStatusBarTexture(ClassPowerTexture)
		mb[i]:SetStatusBarColor(unpack(K.Colors.power.SOUL_SHARDS))

		if i == 1 then
			mb[i]:SetPoint("TOPLEFT", self.Power, "BOTTOMLEFT", 0, -6)
		else
			mb[i]:SetPoint("TOPLEFT", mb[i-1], "TOPRIGHT", 6, 0)
		end
		mb[i].bg = mb[i]:CreateTexture(nil, 'ARTWORK')

		local h = CreateFrame("Frame", nil, mb[i])
		h:SetFrameLevel(5)
		h:SetPoint("TOPLEFT")
		h:SetPoint("BOTTOMRIGHT")
		h:CreateBorder()
	end

	self.SoulShards = mb
end

function Module:CreateBurningEmbersBar()
	local mb = CreateFrame("Frame", "BurningEmbersBar", self)
	mb:SetAllPoints(self.Health)

	for i = 1, 4 do
		mb[i] = CreateFrame("StatusBar", "BurningEmbersBar"..i, self)
		mb[i]:SetSize((self.Health:GetWidth() / 4) -4.5, 16)
		mb[i]:SetFrameLevel(5)
		mb[i]:SetStatusBarTexture(ClassPowerTexture)
		mb[i]:SetStatusBarColor(unpack(K.Colors.power.BURNING_EMBERS))

		if i == 1 then
			mb[i]:SetPoint("TOPLEFT", self.Power, "BOTTOMLEFT", 0, -6)
		else
			mb[i]:SetPoint("TOPLEFT", mb[i-1], "TOPRIGHT", 6, 0)
		end
		mb[i].bg = mb[i]:CreateTexture(nil, 'ARTWORK')

		local h = CreateFrame("Frame", nil, mb[i])
		h:SetFrameLevel(5)
		h:SetPoint("TOPLEFT")
		h:SetPoint("BOTTOMRIGHT")
		h:CreateBorder()
	end

	self.BurningEmbers = mb
end