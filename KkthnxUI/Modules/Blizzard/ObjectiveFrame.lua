local K, C = unpack(select(2, ...))
local Module = K:NewModule("ObjectiveFrame", "AceEvent-3.0", "AceHook-3.0")

local _G = _G
local math_min = math.min

local hooksecurefunc = _G.hooksecurefunc
local GetScreenWidth = _G.GetScreenWidth
local GetScreenHeight = _G.GetScreenHeight

function Module:SetObjectiveFrameHeight()
	local top = WatchFrame:GetTop() or 0
	local screenHeight = GetScreenHeight()
	local gapFromTop = screenHeight - top
	local maxHeight = screenHeight - gapFromTop
	local objectiveFrameHeight = math_min(maxHeight, 480)

	WatchFrame:SetHeight(objectiveFrameHeight)
end

local function IsFramePositionedLeft(frame)
	local x = frame:GetCenter()
	local screenWidth = GetScreenWidth()
	local positionedLeft = false

	if x and x < (screenWidth / 2) then
		positionedLeft = true
	end

	return positionedLeft
end

function Module:MoveObjectiveFrame()
	local Anchor1, Parent, Anchor2, X, Y = "TOPRIGHT", UIParent, "TOPRIGHT", -200, -270
	local Data = KkthnxUIData[K.Realm][K.Name]

	local ObjectiveFrameHolder = CreateFrame("Frame", "TrackerFrameHolder", UIParent)
	ObjectiveFrameHolder:SetSize(130, 22)
	ObjectiveFrameHolder:SetPoint(Anchor1, Parent, Anchor2, X, Y)

	WatchFrame:ClearAllPoints()
	WatchFrame:SetPoint("TOP", ObjectiveFrameHolder, "TOP")
	Module:SetObjectiveFrameHeight()
	WatchFrame:SetClampedToScreen(false)

	-- prevent error from occuring if another addon decides it wants to disable these functions
	WatchFrame.SetMovable = nil
	WatchFrame.SetUserPlaced = nil

	WatchFrame:SetMovable(true)
	WatchFrame:SetUserPlaced(true) -- UIParent.lua line 3090 stops it from being moved <3

	K.Movers:RegisterFrame(ObjectiveFrameHolder)
	K.Movers:SaveDefaults(self, Anchor1, Parent, Anchor2, X, Y)

	if Data and Data.Movers and Data.Movers.TrackerFrameHolder then
		ObjectiveFrameHolder:ClearAllPoints()
		ObjectiveFrameHolder:SetPoint(Data.Movers.TrackerFrameHolder[1], Data.Movers.TrackerFrameHolder[2], Data.Movers.TrackerFrameHolder[3], Data.Movers.TrackerFrameHolder[4], Data.Movers.TrackerFrameHolder[5])
	end
end

function Module:OnEnable()
	if not IsAddOnLoaded("DugisGuideViewerZ") then
		self:MoveObjectiveFrame()
	end
end