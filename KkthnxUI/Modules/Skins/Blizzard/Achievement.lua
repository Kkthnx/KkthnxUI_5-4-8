local K, C = unpack(select(2, ...))
local Module = K:GetModule("Skins")

local _G = _G

local function SkinAchievement()
	if (not IsAddOnLoaded("Blizzard_AchievementUI")) then
		return
	end

	local function SkinStatusBar(bar)
		bar:StripTextures()
		bar:CreateBorder()
		bar:SetStatusBarTexture(C["Media"].Texture)
		bar:SetStatusBarColor(K.Color.r, K.Color.g, K.Color.b)

		local barName = bar:GetName()
		if _G[barName.."Title"] then
			_G[barName.."Title"]:SetPoint("LEFT", 4, 0)
		end

		if _G[barName.."Label"] then
			_G[barName.."Label"]:SetPoint("LEFT", 4, 0)
		end

		if _G[barName.."Text"] then
			_G[barName.."Text"]:SetPoint("RIGHT", -4, 0)
		end
	end

	SkinStatusBar(AchievementFrameSummaryCategoriesStatusBar)
	SkinStatusBar(AchievementFrameComparisonSummaryPlayerStatusBar)
	SkinStatusBar(AchievementFrameComparisonSummaryFriendStatusBar)
	AchievementFrameComparisonSummaryFriendStatusBar.text:ClearAllPoints()
	AchievementFrameComparisonSummaryFriendStatusBar.text:SetPoint("CENTER")
	AchievementFrameComparisonHeader:SetPoint("BOTTOMRIGHT", AchievementFrameComparison, "TOPRIGHT", 45, -10)

	for i = 1, 10 do
		local frame = _G["AchievementFrameSummaryCategoriesCategory"..i]
		local button = _G["AchievementFrameSummaryCategoriesCategory"..i.."Button"]
		local highlight = _G["AchievementFrameSummaryCategoriesCategory"..i.."ButtonHighlight"]

		SkinStatusBar(frame)
		button:StripTextures()
		highlight:StripTextures()
	end
end

Module.SkinFuncs["Blizzard_AchievementUI"] = SkinAchievement