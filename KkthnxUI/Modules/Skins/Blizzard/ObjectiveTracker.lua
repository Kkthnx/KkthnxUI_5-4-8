local K, C = unpack(select(2, ...))
local Module = K:GetModule("Skins")

local _G = _G
local table_insert = table.insert

local _G = _G
local unpack = unpack

local GetNumAutoQuestPopUps = _G.GetNumAutoQuestPopUps
local GetNumQuestWatches = _G.GetNumQuestWatches
local GetQuestDifficultyColor = _G.GetQuestDifficultyColor
local GetQuestIndexForWatch = _G.GetQuestIndexForWatch
local GetQuestLogTitle = _G.GetQuestLogTitle

local function SkinObjectiveTracker()
	-- WatchFrame Expand/Collapse Button
	WatchFrameCollapseExpandButton:SetSize(22, 22)
	WatchFrameCollapseExpandButton:SetNormalTexture("Interface\\AddOns\\KkthnxUI\\Media\\Textures\\TrackerButton")
	WatchFrameCollapseExpandButton:SetPushedTexture("Interface\\AddOns\\KkthnxUI\\Media\\Textures\\TrackerButton")
	WatchFrameCollapseExpandButton:SetHighlightTexture(false or "")
	WatchFrameCollapseExpandButton:SetDisabledTexture("Interface\\AddOns\\KkthnxUI\\Media\\Textures\\TrackerButtonDisabled")
	WatchFrameCollapseExpandButton:SetFrameStrata("MEDIUM")
	WatchFrameCollapseExpandButton:SetPoint("TOPRIGHT", -20, 5)

	WatchFrameHeader:SetPoint("TOPLEFT", 0, -3)

	local headerPanel = _G.CreateFrame("Frame", nil, WatchFrameHeader)
	headerPanel:SetFrameLevel(WatchFrameHeader:GetFrameLevel() - 1)
	headerPanel:SetFrameStrata("BACKGROUND")
	headerPanel:SetPoint("TOPLEFT", 1, 1)
	headerPanel:SetPoint("BOTTOMRIGHT", 1, 1)

	local headerBar = headerPanel:CreateTexture(nil, "ARTWORK")
	headerBar:SetTexture("Interface\\LFGFrame\\UI-LFG-SEPARATOR")
	headerBar:SetTexCoord(0, 0.6640625, 0, 0.3125)
	headerBar:SetVertexColor(K.Colors.class[K.Class][1], K.Colors.class[K.Class][2], K.Colors.class[K.Class][3], K.Colors.class[K.Class][4])
	headerBar:SetPoint("CENTER", headerPanel, 20, -2)
	headerBar:SetSize(200, 30)

	WatchFrameLines:StripTextures()

	hooksecurefunc("WatchFrame_Expand", function()
		if InCombatLockdown() then
			return
		end

		WatchFrameCollapseExpandButton:SetNormalTexture("Interface\\AddOns\\KkthnxUI\\Media\\Textures\\TrackerButton")

		WatchFrame:SetWidth(WATCHFRAME_EXPANDEDWIDTH)
		headerPanel:SetAlpha(1)
	end)

	hooksecurefunc("WatchFrame_Collapse", function()
		if InCombatLockdown() then
			return
		end

		WatchFrameCollapseExpandButton:SetNormalTexture("Interface\\AddOns\\KkthnxUI\\Media\\Textures\\TrackerButton")

		WatchFrame:SetWidth(WATCHFRAME_EXPANDEDWIDTH)
		headerPanel:SetAlpha(0)
	end)

	-- WatchFrame Text
	hooksecurefunc("WatchFrame_Update", function()
		local questIndex
		local numQuestWatches = GetNumQuestWatches()

		for i = 1, numQuestWatches do
			questIndex = GetQuestIndexForWatch(i)
			if questIndex then
				local title, level = GetQuestLogTitle(questIndex)
				local color = GetQuestDifficultyColor(level)

				for j = 1, #WATCHFRAME_QUESTLINES do
					if WATCHFRAME_QUESTLINES[j].text:GetText() == title then
						WATCHFRAME_QUESTLINES[j].text:SetTextColor(color.r, color.g, color.b)
						WATCHFRAME_QUESTLINES[j].color = color
					end
				end

				for k = 1, #WATCHFRAME_ACHIEVEMENTLINES do
					WATCHFRAME_ACHIEVEMENTLINES[k].color = nil
				end
			end
		end

		-- WatchFrame Items
		for i = 1, WATCHFRAME_NUM_ITEMS do
			local button = _G["WatchFrameItem"..i]
			local icon = _G["WatchFrameItem"..i.."IconTexture"]
			local normal = _G["WatchFrameItem"..i.."NormalTexture"]
			local cooldown = _G["WatchFrameItem"..i.."Cooldown"]

			if button and not button.isSkinned then
				button:CreateShadow()
				button:StyleButton()
				button:SetSize(25, 25)

				normal:SetAlpha(0)

				icon:SetInside()
				icon:SetTexCoord(unpack(K.TexCoords))

				button.isSkinned = true
			end
		end
	end)

	-- WatchFrame Highlight
	hooksecurefunc("WatchFrameLinkButtonTemplate_Highlight", function(self, onEnter)
		local line
		for index = self.startLine, self.lastLine do
			line = self.lines[index]
			if line then
				if index == self.startLine then
					if onEnter then
						line.text:SetTextColor(1, 0.80, 0.10)
					else
						if line.color then
							line.text:SetTextColor(line.color.r, line.color.g, line.color.b)
						else
							line.text:SetTextColor(0.75, 0.61, 0)
						end
					end
				end
			end
		end
	end)
end

table_insert(Module.SkinFuncs["KkthnxUI"], SkinObjectiveTracker)