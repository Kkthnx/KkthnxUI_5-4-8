local K, C = unpack(select(2, ...))
local ModuleSkins = K:GetModule("Skins")
local TT = K:GetModule("Tooltip")

local _G = _G
local pairs = pairs

local hooksecurefunc = hooksecurefunc

local function SkinTooltip()
	ItemRefCloseButton:SkinCloseButton()

	local GameTooltip = _G["GameTooltip"]
	local GameTooltipStatusBar = _G["GameTooltipStatusBar"]
	local tooltips = {
		GameTooltip,
		ItemRefTooltip,
		ItemRefShoppingTooltip1,
		ItemRefShoppingTooltip2,
		ItemRefShoppingTooltip3,
		AutoCompleteBox,
		FriendsTooltip,
		ConsolidatedBuffsTooltip,
		ShoppingTooltip1,
		ShoppingTooltip2,
		ShoppingTooltip3,
		WorldMapTooltip,
		WorldMapCompareTooltip1,
		WorldMapCompareTooltip2,
		WorldMapCompareTooltip3
	}

	for _, tt in pairs(tooltips) do
		TT:SecureHookScript(tt, "OnShow", "SetStyle")
	end

	GameTooltipStatusBar:SetStatusBarTexture(C.Media.Texture)
	GameTooltipStatusBar:CreateBorder()
	GameTooltipStatusBar:ClearAllPoints()
	GameTooltipStatusBar:SetPoint("BOTTOMLEFT", GameTooltip, "TOPLEFT", 3, 3)
	GameTooltipStatusBar:SetPoint("BOTTOMRIGHT", GameTooltip, "TOPRIGHT", -3, 3)

	TT:SecureHook("GameTooltip_ShowStatusBar", "GameTooltip_ShowStatusBar")

	TT:SecureHookScript(GameTooltip, "OnSizeChanged", "CheckBackdropColor")
	TT:SecureHookScript(GameTooltip, "OnUpdate", "CheckBackdropColor")
	TT:RegisterEvent("CURSOR_UPDATE", "CheckBackdropColor")
end


table.insert(ModuleSkins.SkinFuncs["KkthnxUI"], SkinTooltip)