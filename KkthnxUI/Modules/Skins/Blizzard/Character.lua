local K = unpack(select(2, ...))
local Module = K:GetModule("Skins")

local function SkinCharacter()
	local slots = {
		"HeadSlot",
		"NeckSlot",
		"ShoulderSlot",
		"BackSlot",
		"ChestSlot",
		"ShirtSlot",
		"TabardSlot",
		"WristSlot",
		"HandsSlot",
		"WaistSlot",
		"LegsSlot",
		"FeetSlot",
		"Finger0Slot",
		"Finger1Slot",
		"Trinket0Slot",
		"Trinket1Slot",
		"MainHandSlot",
		"SecondaryHandSlot"
	}

	for _, slot in pairs(slots) do
		local item = _G["Character"..slot]
		local icon = _G["Character"..slot.."IconTexture"]
		local cooldown = _G["Character"..slot.."Cooldown"]
		local popout = _G["Character"..slot.."PopoutButton"]

		item:CreateBorder(nil, 5, nil, nil)
		icon:SetTexCoord(unpack(K.TexCoords))
		icon:SetInside()

		item:SetFrameLevel(PaperDollFrame:GetFrameLevel() + 2)
	end

	local function ColorItemBorder()
		for _, slot in pairs(slots) do
			local target = _G["Character"..slot]
			local slotId = GetInventorySlotInfo(slot)
			local itemId = GetInventoryItemID("player", slotId)

			if itemId then
				local rarity = GetInventoryItemQuality("player", slotId)
				if rarity then
					target:SetBackdropBorderColor(GetItemQualityColor(rarity))
				else
					target:SetBackdropBorderColor()
				end
			else
				target:SetBackdropBorderColor()
			end
		end
	end

	local CheckItemBorderColor = CreateFrame("Frame")
	CheckItemBorderColor:RegisterEvent("PLAYER_EQUIPMENT_CHANGED")
	CheckItemBorderColor:SetScript("OnEvent", ColorItemBorder)
	_G.CharacterFrame:HookScript("OnShow", ColorItemBorder)
	ColorItemBorder()

	-- Stat Panels
	for i = 1, 7 do
		_G["CharacterStatsPaneCategory"..i]:CreateBorder()
	end
end

table.insert(Module.SkinFuncs["KkthnxUI"], SkinCharacter)