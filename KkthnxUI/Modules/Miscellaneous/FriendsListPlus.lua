local K, C = unpack(select(2, ...))
local Module = K:NewModule("FriendsListPlus")

local _G = _G
local pairs = pairs
local format = format

local IsChatAFK = _G.IsChatAFK
local IsChatDND = _G.IsChatDND
local GetFriendInfo = _G.GetFriendInfo
local GetQuestDifficultyColor = _G.GetQuestDifficultyColor
local GetNumFriends = _G.GetNumFriends
local LEVEL = _G.LEVEL
local FRIENDS_BUTTON_TYPE_WOW = _G.FRIENDS_BUTTON_TYPE_WOW
local LOCALIZED_CLASS_NAMES_FEMALE = _G.LOCALIZED_CLASS_NAMES_FEMALE
local LOCALIZED_CLASS_NAMES_MALE = _G.LOCALIZED_CLASS_NAMES_MALE
local RAID_CLASS_COLORS = _G.RAID_CLASS_COLORS

local Locale = _G.GetLocale()

-- Profile
local FriendsListPlus = {
	["EnhancedTextures"] = true,
	["EnhancedName"] = true,
	["EnhancedZone"] = false,
	["HideClass"] = true,
	["LevelColor"] = false,
	["ShortLevel"] = false,
	["SameZone"] = true,
}

-- Options
local function ColorizeSettingName(settingName)
	return format("|cff1784d1%s|r", settingName);
end

local function ClassColorCode(class)
	for k, v in pairs(LOCALIZED_CLASS_NAMES_MALE) do
		if class == v then
			class = k
		end
	end
	if Locale ~= "enUS" then
		for k, v in pairs(LOCALIZED_CLASS_NAMES_FEMALE) do
			if class == v then
				class = k
			end
		end
	end
	local color = RAID_CLASS_COLORS[class]
	if not color then
		return format("|cFF%02x%02x%02x", 255, 255, 255)
	else
		return format("|cFF%02x%02x%02x", color.r*255, color.g*255, color.b*255)
	end
end

function Module:EnhanceFriends()
	local scrollFrame = FriendsFrameFriendsScrollFrame
	local buttons = scrollFrame.buttons
	local numButtons = #buttons
	local playerZone = GetRealZoneText()

	for i = 1, numButtons do
		local Cooperate = false
		local button = buttons[i]
		local nameText, nameColor, infoText, broadcastText

		if button.buttonType == FRIENDS_BUTTON_TYPE_WOW then
			local name, level, class, area, connected, status = GetFriendInfo(button.id)
			if not name then return end

			broadcastText = nil
			if connected then
				if status == "" then
					button.status:SetTexture(FRIENDS_TEXTURE_ONLINE)
				elseif status == CHAT_FLAG_AFK then
					button.status:SetTexture(FRIENDS_TEXTURE_AFK)
				elseif status == CHAT_FLAG_DND then
					button.status:SetTexture(FRIENDS_TEXTURE_DND)
				end
				local diff = level ~= 0 and format("|cff%02x%02x%02x", GetQuestDifficultyColor(level).r * 255, GetQuestDifficultyColor(level).g * 255, GetQuestDifficultyColor(level).b * 255) or "|cFFFFFFFF"
				local shortLevel = FriendsListPlus.ShortLevel and "SHORT_LEVEL" or LEVEL

				if FriendsListPlus.EnhancedName then
					if FriendsListPlus.HideClass then
						if FriendsListPlus.LevelColor then
							nameText = format("%s%s - %s %s%s|r", ClassColorCode(class), name, shortLevel, diff, level)
						else
							nameText = format("%s%s - %s %s", ClassColorCode(class), name, shortLevel, level)
						end
					else
						if FriendsListPlus.LevelColor then
							nameText = format("%s%s - %s %s%s|r %s%s", ClassColorCode(class), name, shortLevel, diff, level, ClassColorCode(class), class)
						else
							nameText = format("%s%s - %s %s %s", ClassColorCode(class), name, shortLevel, level, class)
						end
					end
				else
					if FriendsListPlus.HideClass then
						if FriendsListPlus.LevelColor then
							nameText = format("%s, %s %s%s|r", name, shortLevel, diff, level)
						else
							nameText = format("%s, %s %s", name, shortLevel, level)
						end

					else
						if FriendsListPlus.LevelColor then
							nameText = format("%s, %s %s%s|r %s", name, shortLevel, diff, level, class)
						else
							nameText = format("%s, %s %s %s", name, shortLevel, level, class)
						end
					end
				end
				nameColor = FRIENDS_WOW_NAME_COLOR
				Cooperate = true
			else
				button.status:SetTexture(FRIENDS_TEXTURE_OFFLINE)
				nameText = name
				nameColor = FRIENDS_GRAY_COLOR
			end
			infoText = area
		end

		if nameText then
			button.name:SetText(nameText)
			button.name:SetTextColor(nameColor.r, nameColor.g, nameColor.b)
			button.info:SetText(infoText)
			button.info:SetTextColor(0.49, 0.52, 0.54)
			if Cooperate then
				if FriendsListPlus.EnhancedZone then
					if FriendsListPlus.SameZone then
						if infoText == playerZone then
							button.info:SetTextColor(0, 1, 0)
						else
							button.info:SetTextColor(1, 0.96, 0.45)
						end
					else
						button.info:SetTextColor(1, 0.96, 0.45)
					end
				else
					if FriendsListPlus.SameZone then
						if infoText == playerZone then
							button.info:SetTextColor(0, 1, 0)
						else
							button.info:SetTextColor(0.49, 0.52, 0.54)
						end
					else
						button.info:SetTextColor(0.49, 0.52, 0.54)
					end
				end
			end
			button.name:SetFont(C["Media"].Font, C["Media"].FontSize, "")
			button.info:SetFont(C["Media"].Font, 11, "")
		end
	end
end

--function EFL:FriendDropdownUpdate()
--	local status
--	if IsChatAFK() then
--		status = FRIENDS_TEXTURE_AFK
--	elseif IsChatDND() then
--		status = FRIENDS_TEXTURE_DND
--	else
--		status = FRIENDS_TEXTURE_ONLINE
--	end

--	FriendsFrameStatusDropDownStatus:SetTexture(status)
--end

function Module:FriendListUpdate()
	hooksecurefunc("HybridScrollFrame_Update", Module.EnhanceFriends)
	hooksecurefunc("FriendsFrame_UpdateFriends", Module.EnhanceFriends)
	--hooksecurefunc("FriendsFrameStatusDropDown_Update", Module.FriendDropdownUpdate)
end

function Module:OnEnable()
	Module:FriendListUpdate()
end