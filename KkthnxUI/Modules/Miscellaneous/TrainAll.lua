local K, C, L = unpack(select(2, ...))
local Module = K:NewModule("TrainAll", "AceHook-3.0", "AceEvent-3.0")

local _G = _G
local select = select

local BuyTrainerService = _G.BuyTrainerService
local GetNumTrainerServices = _G.GetNumTrainerServices
local GetTrainerServiceCost = _G.GetTrainerServiceCost
local GetTrainerServiceInfo = _G.GetTrainerServiceInfo
local ALL, TRAIN = _G.ALL, _G.TRAIN

function Module:ButtonCreate()
	self.button = CreateFrame("Button", "KkthnxUI_TrainAllButton", ClassTrainerFrame, "UIPanelButtonTemplate")
	self.button:SetSize(80, 22)
	self.button:SetText(TRAIN.." "..ALL)
	self.button:SetPoint("TOPRIGHT", ClassTrainerTrainButton, "TOPLEFT", 0, 0)

	self.button:SetScript("OnClick", function()
		for i = 1, GetNumTrainerServices() do
			if select(3, GetTrainerServiceInfo(i)) == "available" then
				BuyTrainerService(i)
			end
		end
	end)

	self.button:HookScript("OnEnter", function()
		local cost = 0
		for i = 1, GetNumTrainerServices() do
			if select(3, GetTrainerServiceInfo(i)) == "available" then
				cost = cost + GetTrainerServiceCost(i)
			end
		end

		GameTooltip:SetOwner(self.button,"ANCHOR_TOPRIGHT", 0, 4)
		GameTooltip:SetText("|cffffffff"..TOTAL.." "..COSTS_LABEL .."|r "..K.FormatMoney(cost))
	end)

	self.button:HookScript("OnLeave", function()
		GameTooltip:Hide()
	end)
end

function Module:ButtonUpdate()
	for i = 1, GetNumTrainerServices() do
		if ClassTrainerTrainButton:IsEnabled() and select(3, GetTrainerServiceInfo(i)) == "available" then
			self.button:Enable()
			return
		end
	end

	self.button:Disable()
end

function Module:ADDON_LOADED(_, addon)
	if addon ~= "Blizzard_TrainerUI" then
		return
	end

	self:ButtonCreate()
	self:UnregisterEvent("ADDON_LOADED")
	self:ToggleState()
end

function Module:ToggleState()
	if self.button then
		self.button:Show()
		if not self:IsHooked("ClassTrainerFrame_Update") then
			self:SecureHook("ClassTrainerFrame_Update", "ButtonUpdate")
		end
	end
end

function Module:OnInitialize()
	self:RegisterEvent("ADDON_LOADED")
	self:ToggleState()
end