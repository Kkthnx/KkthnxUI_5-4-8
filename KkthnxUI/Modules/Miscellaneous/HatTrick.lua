local K, C, L = unpack(select(2, ...))

local _G = _G

local CreateFrame = _G.CreateFrame
local GameTooltip = _G.GameTooltip
local hooksecurefunc = _G.hooksecurefunc
local SHOW_CLOAK = _G.SHOW_CLOAK
local SHOW_HELM = _G.SHOW_HELM

local headCheck = CreateFrame("CheckButton", "HelmCheckBox", PaperDollFrame, "OptionsCheckButtonTemplate")
headCheck:SetSize(15, 15)
headCheck:SkinCheckBox()
headCheck:SetPoint("LEFT", CharacterHeadSlot, "RIGHT", 4, 0)
headCheck:SetFrameStrata("HIGH")
headCheck:SetHitRectInsets(0, 0, 0, 0)

headCheck:SetScript("OnClick", function()
	ShowHelm(not ShowingHelm())
end)

headCheck:SetScript("OnEnter", function()
	GameTooltip:SetOwner(headCheck, "ANCHOR_RIGHT")
	GameTooltip:SetText(SHOW_HELM)
end)

headCheck:SetScript("OnLeave", function()
	GameTooltip:Hide()
end)

local cloakCheck = CreateFrame("CheckButton", "CloakCheckBox", PaperDollFrame, "OptionsCheckButtonTemplate")
cloakCheck:SetSize(15, 15)
cloakCheck:SkinCheckBox()
cloakCheck:SetPoint("LEFT", CharacterBackSlot, "RIGHT", 4, 0)
cloakCheck:SetFrameStrata("HIGH")
cloakCheck:SetHitRectInsets(0, 0, 0, 0)

cloakCheck:SetScript("OnClick", function()
	ShowCloak(not ShowingCloak())
end)

cloakCheck:SetScript("OnEnter", function()
	GameTooltip:SetOwner(cloakCheck, "ANCHOR_RIGHT")
	GameTooltip:SetText(SHOW_CLOAK)
end)

cloakCheck:SetScript("OnLeave", function()
	GameTooltip:Hide()
end)

hooksecurefunc("ShowHelm", function(headValue)
	headCheck:SetChecked(headValue)
end)

hooksecurefunc("ShowCloak", function(cloakValue)
	cloakCheck:SetChecked(cloakValue)
end)

local eventFrame = CreateFrame("Frame")
eventFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
eventFrame:SetScript("OnEvent", function(self)
	self:UnregisterEvent("PLAYER_ENTERING_WORLD")

	headCheck:SetChecked(ShowingHelm())
	cloakCheck:SetChecked(ShowingCloak())
end)