local K, C, L = unpack(select(2, ...))
local Unfit = LibStub("Unfit-1.0")
local ModuleSort = K:GetModule("InventorySort")
local ModuleVendor = K:GetModule("Vendor")

if C["Inventory"].Enable ~= true
or IsAddOnLoaded("AdiBags")
or IsAddOnLoaded("ArkInventory")
or IsAddOnLoaded("cargBags_Nivaya")
or IsAddOnLoaded("cargBags")
or IsAddOnLoaded("Bagnon")
or IsAddOnLoaded("Combuctor")
or IsAddOnLoaded("TBag")
or IsAddOnLoaded("BaudBag") then
	return
end

-- Sourced (by Hungtar, editor Tukz then Kkthnx)

local _G = _G
local bit_band = bit.band
local ipairs = ipairs
local math_floor = math.floor
local pairs = pairs
local select = select
local string_find = string.find
local string_gsub = string.gsub
local string_lower = string.lower
local string_match = string.match
local table_insert = table.insert
local table_remove = table.remove
local tonumber = tonumber

local BankFrameItemButton_Update = _G.BankFrameItemButton_Update
local BankFrameItemButton_UpdateLocked = _G.BankFrameItemButton_UpdateLocked
local C_NewItems_IsNewItem = _G.C_NewItems.IsNewItem
local CloseBankFrame = _G.CloseBankFrame
--local CooldownFrame_Set = _G.CooldownFrame_Set
local CreateFrame = _G.CreateFrame
local GameTooltip = _G.GameTooltip
local GetContainerItemCooldown = _G.GetContainerItemCooldown
local GetContainerItemEquipmentSetInfo = _G.GetContainerItemEquipmentSetInfo
local GetContainerItemID = _G.GetContainerItemID
local GetContainerItemInfo = _G.GetContainerItemInfo
local GetContainerItemLink = _G.GetContainerItemLink
local GetContainerItemQuestInfo = _G.GetContainerItemQuestInfo
local GetContainerNumFreeSlots = _G.GetContainerNumFreeSlots
local GetContainerNumSlots = _G.GetContainerNumSlots
local GetItemInfo = _G.GetItemInfo
local GetItemQualityColor = _G.GetItemQualityColor
local GetMoney = _G.GetMoney
local GetNumBankSlots = _G.GetNumBankSlots
local GetReagentBankCost = _G.GetReagentBankCost
local IsBattlePayItem = _G.IsBattlePayItem
local IsShiftKeyDown = _G.IsShiftKeyDown
local LE_ITEM_QUALITY_POOR = _G.LE_ITEM_QUALITY_POOR
local MoneyFrame_Update = _G.MoneyFrame_Update
local NEW_ITEM_ATLAS_BY_QUALITY = _G.NEW_ITEM_ATLAS_BY_QUALITY
local PlaySound = _G.PlaySound
local SEARCH = _G.SEARCH
local SetItemButtonCount = _G.SetItemButtonCount
local SetItemButtonDesaturated = _G.SetItemButtonDesaturated
local SetItemButtonTexture = _G.SetItemButtonTexture
local SortReagentBankBags = _G.SortReagentBankBags
local Token1, Token2, Token3 = _G.BackpackTokenFrameToken1, _G.BackpackTokenFrameToken2, _G.BackpackTokenFrameToken3
local UIParent = _G.UIParent
local hooksecurefunc = _G.hooksecurefunc

local BAGS_FONT = K.GetFont(C["Inventory"].Font)
local BAGS_BACKPACK = {0, 1, 2, 3, 4}
local BAGS_BANK = {-1, 5, 6, 7, 8, 9, 10, 11}
local ST_NORMAL = 1
local ST_FISHBAG = 2
local ST_SPECIAL = 3
local bag_bars = 0
local unusable

if K.Class == "DEATHKNIGHT" then
	unusable = {{3, 4, 10, 11, 13, 14, 15, 16}, {6}}
elseif K.Class == "DRUID" then
	unusable = {{1, 2, 3, 4, 8, 9, 14, 15, 16}, {4, 5, 6}, true}
elseif K.Class == "HUNTER" then
	unusable = {{5, 6, 16}, {5, 6}}
elseif K.Class == "MAGE" then
	unusable = {{1, 2, 3, 4, 5, 6, 7, 9, 11, 14, 15}, {3, 4, 5, 6}, true}
elseif K.Class == "MONK" then
	unusable = {{2, 3, 4, 6, 9, 13, 14, 15, 16}, {4, 5, 6}}
elseif K.Class == "PALADIN" then
	unusable = {{3, 4, 10, 11, 13, 14, 15, 16}, {}, true}
elseif K.Class == "PRIEST" then
	unusable = {{1, 2, 3, 4, 6, 7, 8, 9, 11, 14, 15}, {3, 4, 5, 6}, true}
elseif K.Class == "ROGUE" then
	unusable = {{2, 6, 7, 9, 10, 16}, {4, 5, 6}}
elseif K.Class == "SHAMAN" then
	unusable = {{3, 4, 7, 8, 9, 14, 15, 16}, {5}}
elseif K.Class == "WARLOCK" then
	unusable = {{1, 2, 3, 4, 5, 6, 7, 9, 11, 14, 15}, {3, 4, 5, 6}, true}
elseif K.Class == "WARRIOR" then
	unusable = {{16}, {}}
end

for class = 1, 2 do
	local subs = {GetAuctionItemSubClasses(class)}
	for i, subclass in ipairs(unusable[class]) do
		unusable[subs[subclass]] = true
	end
	unusable[class] = nil
	subs = nil
end

local function IsClassUnusable(subclass, slot)
	if subclass then
		return unusable[subclass] or slot == "INVTYPE_WEAPONOFFHAND" and unusable[3]
	end
end

local function IsItemUnusable(...)
	if ... then
		local subclass, _, slot = select(7, GetItemInfo(...))
		return IsClassUnusable(subclass, slot)
	end
end

local function GetGraysValue()
	local value = 0

	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)
			if itemID then
				local _, _, rarity, _, _, itype, _, _, _, _, itemPrice = GetItemInfo(itemID)
				if itemPrice then
					local stackCount = select(2, GetContainerItemInfo(bag, slot)) or 1
					local stackPrice = itemPrice * stackCount
					if (rarity and rarity == 0) and (itype and itype ~= "Quest") and (stackPrice > 0) then
						value = value + stackPrice
					end
				end
			end
		end
	end

	return value
end

local function VendorGrayCheck()
	local value = GetGraysValue()

	if value == 0 then
		K.Print("No gray items to delete.")
	elseif not MerchantFrame or not MerchantFrame:IsShown() then
		K.PopupDialogs["DELETE_GRAYS"].Money = value
		K.StaticPopup_Show('DELETE_GRAYS')
	else
		ModuleVendor:VendorGrays()
	end
end

local Stuffing = CreateFrame("Frame", nil, UIParent)
Stuffing:RegisterEvent("ADDON_LOADED")
Stuffing:RegisterEvent("PLAYER_ENTERING_WORLD")
Stuffing:SetScript("OnEvent", function(this, event, ...)
	Stuffing[event](this, ...)
end)

local function Stuffing_OnShow()
	Stuffing:PLAYERBANKSLOTS_CHANGED(29)

	for i = 0, #BAGS_BACKPACK - 1 do
		Stuffing:BAG_UPDATE(i)
	end

	Stuffing:Layout()
	Stuffing:SearchReset()
	PlaySound("igbackpackopen")

	collectgarbage("collect")
end

local function StuffingBank_OnHide()
	CloseBankFrame()

	if Stuffing.frame:IsShown() then
		Stuffing.frame:Hide()
	end

	if bag_bars == 1 then
		bag_bars = 0
	end

	PlaySound("igbackpackclose")
end

local function Stuffing_OnHide()
	if Stuffing.bankFrame and Stuffing.bankFrame:IsShown() then
		Stuffing.bankFrame:Hide()
	end

	if bag_bars == 1 then
		bag_bars = 0
	end

	PlaySound("igbackpackclose")
end

local function Stuffing_Open()
	if not Stuffing.frame:IsShown() then
		Stuffing.frame:Show()
	end
end

local function Stuffing_Close()
	Stuffing.frame:Hide()
end

local function Stuffing_Toggle()
	if Stuffing.frame:IsShown() then
		Stuffing.frame:Hide()
	else
		Stuffing.frame:Show()
	end
end

-- Bag slot stuff
local trashButton = {}
local trashBag = {}

-- Tooltip and scanning by Phanx @ http://www.wowinterface.com/forums/showthread.php?p=271406
local S_ITEM_LEVEL = "^" .. string_gsub(_G.ITEM_LEVEL, "%%d", "(%%d+)")

local ItemDB = {}
local function IsRealItemLevel(link, owner, bag, slot)
	if ItemDB[link] then
		return ItemDB[link]
	end

	local realItemLevel

	K.ScanTooltip.owner = owner
	K.ScanTooltip:SetOwner(_G.UIParent, "ANCHOR_NONE")
	K.ScanTooltip:SetBagItem(bag, slot)

	local line = _G[K.ScanTooltip:GetName() .. "TextLeft2"]
	if line then
		local msg = line:GetText()
		if msg and string_find(msg, S_ITEM_LEVEL) then
			local itemLevel = string_match(msg, S_ITEM_LEVEL)
			if itemLevel and (tonumber(itemLevel) > 0) then
				realItemLevel = itemLevel
			end
		else
			-- Check line 3, some artifacts have the ilevel there
			line = _G[K.ScanTooltip:GetName() .. "TextLeft3"]
			if line then
				local msg = line:GetText()
				if msg and string_find(msg, S_ITEM_LEVEL) then
					local itemLevel = string_match(msg, S_ITEM_LEVEL)
					if itemLevel and (tonumber(itemLevel) > 0) then
						realItemLevel = itemLevel
					end
				end
			end
		end
	end

	ItemDB[link] = tonumber(realItemLevel)
	return realItemLevel
end

function Stuffing:SlotUpdate(b)
	local texture, count, locked = GetContainerItemInfo(b.bag, b.slot)
	local clink = GetContainerItemLink(b.bag, b.slot)
	local isQuestItem, questId, isActiveQuest = GetContainerItemQuestInfo(b.bag, b.slot)

	if not b.frame.lock then
		b.frame:SetBackdropBorderColor()
	end

	if b.Cooldown and StuffingFrameBags and StuffingFrameBags:IsShown() then
		local start, duration, enable = GetContainerItemCooldown(b.bag, b.slot)
		CooldownFrame_SetTimer(b.Cooldown, start, duration, enable)
	end

	if C["Inventory"].ItemLevel == true then
		b.frame.text:SetText("")
	end

	if (b.frame.JunkIcon) then
		b.frame.JunkIcon:Hide()
	end

	if b.frame.UpgradeIcon then
		b.frame.UpgradeIcon:ClearAllPoints()
		b.frame.UpgradeIcon:SetTexture("Interface\\AddOns\\KkthnxUI\\Media\\Inventory\\UpgradeIcon")
		b.frame.UpgradeIcon:SetPoint("BOTTOMRIGHT", 6, -3)
		b.frame.UpgradeIcon:SetSize(C["Inventory"].ButtonSize / 1.4, C["Inventory"].ButtonSize / 1.4)
		b.frame.UpgradeIcon:SetTexCoord(0, 1, 0, 1)

		local itemIsUpgrade = _G.IsContainerItemAnUpgrade(b.frame:GetParent():GetID(), b.frame:GetID())
		if not itemIsUpgrade or itemIsUpgrade == nil then
			b.frame.UpgradeIcon:SetShown(false)
		else
			b.frame.UpgradeIcon:SetShown(itemIsUpgrade or true)
		end
	end

	local questTexture = _G[b.frame:GetName() .. "IconQuestTexture"]
	if questTexture then
		questTexture:ClearAllPoints()
		if questId and not isActiveQuest then
			questTexture:SetTexture("Interface\\AddOns\\KkthnxUI\\Media\\Inventory\\QuestIcon.tga")
			questTexture:SetPoint("BOTTOMLEFT", 1, 1)
			questTexture:SetSize(C["Inventory"].ButtonSize / 1.2, C["Inventory"].ButtonSize / 1.2)
			questTexture:SetTexCoord(0, 1, 0, 1)
			questTexture:Show()
		else
			questTexture:Hide()
		end
	end

	if IsAddOnLoaded("CanIMogIt") then
		CIMI_AddToFrame(b.frame, ContainerFrameItemButton_CIMIUpdateIcon)
		ContainerFrameItemButton_CIMIUpdateIcon(b.frame.CanIMogItOverlay)
	end

	if clink then
		b.name, _, b.Rarity, _, b.level, b.Type, _, _, b.EquipLoc, _, b.SellPrice = GetItemInfo(clink)

		if C["Inventory"].ItemLevel == true and (b.EquipLoc ~= nil and b.EquipLoc ~= "" and b.EquipLoc ~= "INVTYPE_BAG" and b.EquipLoc ~= "INVTYPE_QUIVER" and b.EquipLoc ~= "INVTYPE_TABARD") and (b.Rarity and b.Rarity > 1) then
			b.itemlevel = IsRealItemLevel(clink, self, b.bag, b.slot)
			b.frame.text:SetText(b.itemlevel)
			b.frame.text:SetTextColor(GetItemQualityColor(b.Rarity))
		end

		if (b.frame.JunkIcon) then
			if C["Inventory"].JunkIcon and (b.Rarity and b.Rarity == 0) and (b.SellPrice and b.SellPrice > 0) and (b.Type and b.Type ~= "Quest") then
				b.frame.JunkIcon:Show()
			else
				b.frame.JunkIcon:Hide()
			end
		end

		if (IsItemUnusable(clink) or b.level and b.level > K.Level) and not locked then
			_G[b.frame:GetName().."IconTexture"]:SetVertexColor(1, 0.1, 0.1)
		else
			_G[b.frame:GetName().."IconTexture"]:SetVertexColor(1, 1, 1)
		end

		-- Color slot according to item quality
		if not b.frame.lock and questId and not isActiveQuest then
			b.frame:SetBackdropBorderColor(1.0, 0.3, 0.3)
			if (b.frame.questIcon) then
				b.frame.questIcon:Show()
			end
		elseif questId or isQuestItem then
			b.frame:SetBackdropBorderColor(1.0, 0.3, 0.3)
		elseif b.Rarity and b.Rarity > 1 then
			b.frame:SetBackdropBorderColor(GetItemQualityColor(b.Rarity))
		else
			b.frame:SetBackdropBorderColor()
			b.name, b.level = nil, nil
		end

		-- Color slot according to item quality
		if not b.frame.lock and b.Rarity and b.Rarity > 1 and not (isQuestItem or questId) then
			b.frame:SetBackdropBorderColor(GetItemQualityColor(b.Rarity))
		elseif isQuestItem or questId then
			b.frame:SetBackdropBorderColor(1, 1, 0)
		end
	else
		b.name, b.level = nil, nil
	end

	SetItemButtonTexture(b.frame, texture)
	SetItemButtonCount(b.frame, count)
	SetItemButtonDesaturated(b.frame, locked)

	b.frame:Show()
end

function Stuffing:BagSlotUpdate(bag)
	if not self.buttons then
		return
	end

	for _, v in ipairs(self.buttons) do
		if v.bag == bag then
			self:SlotUpdate(v)
		end
	end
end

function Stuffing:UpdateCooldowns(b)
	if b.cooldown and StuffingFrameBags and StuffingFrameBags:IsShown() then
		local start, duration, enable = GetContainerItemCooldown(b.bag, b.slot)
		CooldownFrame_SetTimer(b.Cooldown, start, duration, enable)
	end
end

local function Stuffing_TooltipHide()
	if not GameTooltip:IsForbidden() then
		GameTooltip:Hide()
	end
end

local function Stuffing_TooltipShow(self)
	GameTooltip:SetOwner(self)
	GameTooltip:ClearLines()
	GameTooltip:AddLine(self.ttText)

	GameTooltip:Show()
end

function Stuffing:BagFrameSlotNew(p, slot)
	for _, v in ipairs(self.bagframe_buttons) do
		if v.slot == slot then
			return v, false
		end
	end

	local ret = {}

	if slot > 3 then
		ret.slot = slot
		slot = slot - 4
		ret.frame = CreateFrame("CheckButton", "StuffingBBag" .. slot, p, "BankItemButtonBagTemplate")
		ret.frame:StripTextures()
		ret.frame:SetID(slot)

		table_insert(self.bagframe_buttons, ret)

		BankFrameItemButton_Update(ret.frame)
		BankFrameItemButton_UpdateLocked(ret.frame)

		if not ret.frame.tooltipText then
			ret.frame.tooltipText = ""
		end

		if slot > GetNumBankSlots() then
			SetItemButtonTextureVertexColor(ret.frame, 1.0, 0.1, 0.1)
		else
			SetItemButtonTextureVertexColor(ret.frame, 1.0, 1.0, 1.0)
		end
	else
		ret.frame = CreateFrame("CheckButton", "StuffingFBag" .. slot .. "Slot", p, "BagSlotButtonTemplate")

		ret.slot = slot
		table_insert(self.bagframe_buttons, ret)
	end

	ret.frame:CreateBorder()
	ret.frame:StyleButton()
	ret.frame:SetNormalTexture("")
	ret.frame:SetCheckedTexture("")

	ret.icon = _G[ret.frame:GetName() .. "IconTexture"]
	ret.icon:SetTexCoord(0.1, 0.9, 0.1, 0.9)
	ret.icon:SetAllPoints()

	return ret
end

function Stuffing:SlotNew(bag, slot)
	for _, v in ipairs(self.buttons) do
		if v.bag == bag and v.slot == slot then
			v.lock = false
			return v, false
		end
	end

	local tpl = "ContainerFrameItemButtonTemplate"

	if bag == -1 then
		tpl = "BankItemButtonGenericTemplate"
	end

	local ret = {}

	if #trashButton > 0 then
		local f = -1
		for i, v in ipairs(trashButton) do
			local b, s = v:GetName():match("(%d+)_(%d+)")

			b = tonumber(b)
			s = tonumber(s)

			if b == bag and s == slot then
				f = i
				break
			else
				v:Hide()
			end
		end

		if f ~= -1 then
			ret.frame = trashButton[f]
			table_remove(trashButton, f)
			ret.frame:Show()
		end
	end

	if not ret.frame then
		ret.frame = CreateFrame("Button", "StuffingBag" .. bag .. "_" .. slot, self.bags[bag], tpl)

		ret.frame:CreateBorder()
		ret.frame:StyleButton()
		ret.frame:SetNormalTexture(nil)

		ret.icon = _G[ret.frame:GetName() .. "IconTexture"]
		ret.icon:SetTexCoord(0.1, 0.9, 0.1, 0.9)
		ret.icon:SetAllPoints()

		ret.count = _G[ret.frame:GetName() .. "Count"]
		ret.count:SetFontObject(BAGS_FONT)
		ret.count:SetShadowOffset(0, 0)
		ret.count:SetPoint("BOTTOMRIGHT", 1, 1)

		if C["Inventory"].ItemLevel == true then
			ret.frame.text = ret.frame:CreateFontString(nil, 'OVERLAY')
			ret.frame.text:SetPoint("TOPLEFT", 1, -1)
			ret.frame.text:FontTemplate(nil, 11, "OUTLINE")
		end

		-- JunkIcon only exists for items created through ContainerFrameItemButtonTemplate
		if not ret.frame.JunkIcon then
			ret.frame.JunkIcon = ret.frame:CreateTexture(nil, "OVERLAY")
			ret.frame.JunkIcon:SetTexture("Interface\\AddOns\\KkthnxUI\\Media\\Inventory\\JunkIcon.blp")
			ret.frame.JunkIcon:SetPoint("TOPLEFT", 1, 0)
			ret.frame.JunkIcon:Hide()
		end

		if _G[ret.frame:GetName().."NewItemTexture"] then
			_G[ret.frame:GetName().."NewItemTexture"]:SetAlpha(0)
		end
	end

	ret.bag = bag
	ret.slot = slot
	ret.frame:SetID(slot)

	ret.cooldown = _G[ret.frame:GetName() .. "Cooldown"]
	ret.cooldown:Show()

	self:SlotUpdate(ret)

	return ret, true
end

local BAGTYPE_PROFESSION = 0x0008 + 0x0010 + 0x0020 + 0x0040 + 0x0080 + 0x0200 + 0x0400 + 0x10000
local BAGTYPE_FISHING = 32768

function Stuffing:BagType(bag)
	local bagType = select(2, GetContainerNumFreeSlots(bag))

	if bagType and bit_band(bagType, BAGTYPE_FISHING) > 0 then
		return ST_FISHBAG
	elseif bagType and bit_band(bagType, BAGTYPE_PROFESSION) > 0 then
		return ST_SPECIAL
	end

	return ST_NORMAL
end

function Stuffing:BagNew(bag, f)
	for _, v in pairs(self.bags) do
		if v:GetID() == bag then
			v.bagType = self:BagType(bag)
			return v
		end
	end

	local ret

	if #trashBag > 0 then
		local f = -1
		for i, v in pairs(trashBag) do
			if v:GetID() == bag then
				f = i
				break
			end
		end

		if f ~= -1 then
			ret = trashBag[f]
			table_remove(trashBag, f)
			ret:Show()
			ret.bagType = self:BagType(bag)
			return ret
		end
	end

	ret = CreateFrame("Frame", "StuffingBag" .. bag, f)
	ret.bagType = self:BagType(bag)

	ret:SetID(bag)
	return ret
end

function Stuffing:SearchUpdate(str)
	str = string_lower(str)

	for _, b in ipairs(self.buttons) do
		if b.frame and not b.name then
			b.frame:SetAlpha(0.2)
		end
		if b.name then
			local _, setName = GetContainerItemEquipmentSetInfo(b.bag, b.slot)

			setName = setName or ""

			local ilink = GetContainerItemLink(b.bag, b.slot)
			local class, subclass, _, equipSlot = select(6, GetItemInfo(ilink))
			local minLevel = select(5, GetItemInfo(ilink))

			class = _G[class] or ""
			subclass = _G[subclass] or ""
			equipSlot = _G[equipSlot] or ""
			minLevel = minLevel or 1

			if not string_find(string_lower(b.name), str) and not string_find(string_lower(setName), str) and not string_find(string_lower(class), str) and not string_find(string_lower(subclass), str) and not string_find(string_lower(equipSlot), str) then
				if IsItemUnusable(b.name) or minLevel > K.Level then
					_G[b.frame:GetName() .. "IconTexture"]:SetVertexColor(0.5, 0.5, 0.5)
				end

				SetItemButtonDesaturated(b.frame, true)
				b.frame:SetAlpha(0.2)
			else
				if IsItemUnusable(b.name) or minLevel > K.Level then
					_G[b.frame:GetName() .. "IconTexture"]:SetVertexColor(1, 0.1, 0.1)
				end

				SetItemButtonDesaturated(b.frame, false)
				b.frame:SetAlpha(1)
			end
		end
	end
end

function Stuffing:SearchReset()
	for _, b in ipairs(self.buttons) do
		if IsItemUnusable(b.name) or (b.level and b.level > K.Level) then
			_G[b.frame:GetName() .. "IconTexture"]:SetVertexColor(1, 0.1, 0.1)
		end

		b.frame:SetAlpha(1)
		SetItemButtonDesaturated(b.frame, false)
	end
end

local function DragFunction(self, mode)
	for index = 1, select("#", self:GetChildren()) do
		local frame = select(index, self:GetChildren())
		if frame:GetName() and frame:GetName():match("StuffingBag") then
			if mode then
				frame:Hide()
			else
				frame:Show()
			end
		end
	end
end

function Stuffing:CreateBagFrame(w)
	local n = "StuffingFrame" .. w
	local f = CreateFrame("Frame", n, UIParent)
	f:EnableMouse(true)
	f:SetMovable(true)
	f:SetFrameStrata("MEDIUM")
	f:SetFrameLevel(5)
	f:RegisterForDrag("LeftButton", "RightButton")
	f:SetScript("OnDragStart", function(self)
		if IsShiftKeyDown() then
			self:StartMoving()
			DragFunction(self, true)
		end
	end)

	f:SetScript("OnDragStop", function(self)
		self:StopMovingOrSizing()
		DragFunction(self, false)
	end)

	f:SetScript("OnEnter", function(self)
		if GameTooltip:IsForbidden() then
			return
		end

		GameTooltip:SetOwner(self, "ANCHOR_TOPLEFT", 0, 4)
		GameTooltip:ClearLines()
		GameTooltip:AddDoubleLine(L["Inventory"].Shift_Move)

		GameTooltip:Show()
	end)

	f:SetScript("OnLeave", function()
		if not GameTooltip:IsForbidden() then
			GameTooltip:Hide()
		end
	end)

	if w == "Bank" then
		f:SetPoint("BOTTOMLEFT", "UIParent", "BOTTOMLEFT", 4, 204)
	else
		f:SetPoint("BOTTOMRIGHT", "UIParent", "BOTTOMRIGHT", -4, 204)
	end

	if w == "Bank" then
		f.reagentToggle = CreateFrame("Button", "StuffingReagentButton" .. w, f)
		f.reagentToggle:SetSize(16, 16)
		f.reagentToggle:CreateBorder()
		f.reagentToggle:SetPoint("TOPRIGHT", f, -32, -7)
		f.reagentToggle:SetNormalTexture("Interface\\ICONS\\INV_Enchant_DustArcane")
		f.reagentToggle:GetNormalTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.reagentToggle:GetNormalTexture():SetAllPoints()
		f.reagentToggle:SetPushedTexture("Interface\\ICONS\\INV_Enchant_DustArcane")
		f.reagentToggle:GetPushedTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.reagentToggle:GetPushedTexture():SetAllPoints()
		f.reagentToggle:StyleButton(nil, true)
		f.reagentToggle.ttText = L["Inventory"].Reagents
		f.reagentToggle:SetScript("OnEnter", Stuffing_TooltipShow)
		f.reagentToggle:SetScript("OnLeave", Stuffing_TooltipHide)
		f.reagentToggle:SetScript("OnClick", function()
			BankFrame_ShowPanel(BANK_PANELS[2].name)
			PlaySound("igcharacterinfotab")
			if not ReagentBankFrame.isMade then
				Stuffing_CreateReagentContainer()
				ReagentBankFrame.isMade = true
			else
				_G["StuffingFrameReagent"]:Show()
			end

			_G["StuffingFrameBank"]:SetAlpha(0)
		end)

		f.bagsButton = CreateFrame("Button", nil, f)
		f.bagsButton:SetSize(16, 16)
		f.bagsButton:CreateBorder()
		f.bagsButton:SetPoint("RIGHT", f.reagentToggle, "LEFT", -5, 0)
		f.bagsButton:SetNormalTexture("Interface\\Buttons\\Button-Backpack-Up")
		f.bagsButton:GetNormalTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.bagsButton:GetNormalTexture():SetAllPoints()
		f.bagsButton:SetPushedTexture("Interface\\Buttons\\Button-Backpack-Up")
		f.bagsButton:GetPushedTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.bagsButton:GetPushedTexture():SetAllPoints()
		f.bagsButton:StyleButton(nil, true)
		f.bagsButton.ttText = L["Inventory"].Show_Bags
		f.bagsButton:SetScript("OnEnter", Stuffing_TooltipShow)
		f.bagsButton:SetScript("OnLeave", Stuffing_TooltipHide)
		f.bagsButton:SetScript("OnClick", function()
			PlaySound("igmainmenuoption")
			if bag_bars == 1 then
				bag_bars = 0
			else
				bag_bars = 1
			end

			if Stuffing.bankFrame and Stuffing.bankFrame:IsShown() then
				Stuffing:Layout(true)
			end
		end)

		f.sortButton = CreateFrame("Button", nil, f)
		f.sortButton:SetSize(16, 16)
		f.sortButton:CreateBorder()
		f.sortButton:StyleButton(true)
		f.sortButton:SetPoint("TOPRIGHT", f.bagsButton, -22, 0)
		f.sortButton:SetNormalTexture("Interface\\ICONS\\INV_Pet_Broom")
		f.sortButton:GetNormalTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.sortButton:GetNormalTexture():SetAllPoints()
		f.sortButton:SetPushedTexture("Interface\\ICONS\\INV_Pet_Broom")
		f.sortButton:GetPushedTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.sortButton:GetPushedTexture():SetAllPoints()
		f.sortButton:SetDisabledTexture("Interface\\ICONS\\INV_Pet_Broom")
		f.sortButton:GetDisabledTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.sortButton:GetDisabledTexture():SetAllPoints()
		f.sortButton:GetDisabledTexture():SetDesaturated(1)
		f.sortButton.ttText = BAG_FILTER_CLEANUP
		f.sortButton:SetScript("OnEnter", Stuffing_TooltipShow)
		f.sortButton:SetScript("OnLeave", Stuffing_TooltipHide)
		f.sortButton:SetScript("OnMouseUp", function()
			-- if StuffingFrameBags and StuffingFrameBags:IsShown() then
			if Stuffing.frame:IsShown() then
				f:UnregisterAllEvents() -- Unregister to prevent unnecessary updates
				f.registerUpdate = true -- Set variable that indicates this bag should be updated when sorting is done
				ModuleSort:CommandDecorator(ModuleSort.SortBags, "bank")()
			end
		end)

		f.purchaseBagButton = CreateFrame("Button", "StuffingPurchaseButton" .. w, f)
		f.purchaseBagButton:SetSize(16, 16)
		f.purchaseBagButton:CreateBorder()
		f.purchaseBagButton:SetPoint("RIGHT", f.sortButton, "LEFT", -5, 0)
		f.purchaseBagButton:SetNormalTexture("Interface\\ICONS\\INV_Misc_Coin_01")
		f.purchaseBagButton:GetNormalTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.purchaseBagButton:GetNormalTexture():SetAllPoints()
		f.purchaseBagButton:SetPushedTexture("Interface\\ICONS\\INV_Misc_Coin_01")
		f.purchaseBagButton:GetPushedTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.purchaseBagButton:GetPushedTexture():SetAllPoints()
		f.purchaseBagButton:StyleButton(nil, true)
		f.purchaseBagButton.ttText = L["Inventory"].Purchase_Slot
		f.purchaseBagButton:SetScript("OnEnter", Stuffing_TooltipShow)
		f.purchaseBagButton:SetScript("OnLeave", Stuffing_TooltipHide)
		f.purchaseBagButton:SetScript("OnClick", function()
			local numSlots, full = GetNumBankSlots()

			if (full) then
				K.StaticPopup_Show("CANNOT_BUY_BANK_SLOT")
			else
				K.StaticPopup_Show("BUY_BANK_SLOT")
			end

			local button
			for i = 1, NUM_BANKBAGSLOTS, 1 do
				button = _G["StuffingBBag"..i.."Slot"]
				if button then
					if i <= numSlots then
						SetItemButtonTextureVertexColor(button, 1.0, 1.0, 1.0)
					else
						SetItemButtonTextureVertexColor(button, 1.0, 0.1, 0.1)
					end
				end
			end
		end)
	end

	f.b_close = CreateFrame("Button", "StuffingCloseButton" .. w, f, "UIPanelCloseButton")
	f.b_close:SetPoint("TOPRIGHT", 0, 1)
	f.b_close:SkinCloseButton()
	f.b_close:RegisterForClicks("AnyUp")
	f.b_close:SetScript("OnClick", function(self)
		self:GetParent():Hide()
	end)

	local fb = CreateFrame("Frame", n .. "BagsFrame", f)
	fb:SetPoint("BOTTOMLEFT", f, "TOPLEFT", 0, 6)
	fb:SetFrameStrata("MEDIUM")
	f.bags_frame = fb

	return f
end

function Stuffing:InitBank()
	if self.bankFrame then
		return
	end

	local f = self:CreateBagFrame("Bank")
	f:SetScript("OnHide", StuffingBank_OnHide)
	self.bankFrame = f
end

function Stuffing:InitBags()
	if self.frame then
		return
	end

	self.buttons = {}
	self.bags = {}
	self.bagframe_buttons = {}

	local f = self:CreateBagFrame("Bags")
	f:SetScript("OnShow", Stuffing_OnShow)
	f:SetScript("OnHide", Stuffing_OnHide)

	local editbox = CreateFrame("EditBox", nil, f)
	editbox:Hide()
	editbox:SetAutoFocus(true)
	editbox:SetHeight(32)
	editbox:CreateBackdrop()
	editbox.Backdrop:SetPoint("TOPLEFT", -2, 2)
	editbox.Backdrop:SetPoint("BOTTOMRIGHT", 2, -2)

	local function resetAndClear(self)
		self:GetParent().detail:Show()
		self:GetParent().gold:Show()
		self:ClearFocus()
		Stuffing:SearchReset()
	end

	local function updateSearch(self, t)
		if t == true then
			Stuffing:SearchUpdate(self:GetText())
		end
	end

	editbox:SetScript("OnEscapePressed", resetAndClear)
	editbox:SetScript("OnEnterPressed", resetAndClear)
	editbox:SetScript("OnEditFocusLost", editbox.Hide)
	editbox:SetScript("OnEditFocusGained", editbox.HighlightText)
	editbox:SetScript("OnTextChanged", updateSearch)
	editbox:SetScript("OnChar", updateSearch)

	local detail = f:CreateFontString(nil, "ARTWORK", "GameFontHighlightLarge")
	detail:SetPoint("TOPLEFT", f, 11, -10)
	detail:SetPoint("RIGHT", f, -140, -10)
	detail:SetHeight(14)
	detail:SetShadowColor(0, 0, 0, 0)
	detail:SetJustifyH("LEFT")
	detail:SetText(SEARCH)
	editbox:SetAllPoints(detail)

	local gold = f:CreateFontString(nil, "OVERLAY")
	gold:FontTemplate(nil, nil, "OUTLINE")
	gold:SetJustifyH("RIGHT")

	f:RegisterEvent("PLAYER_ENTERING_WORLD")
	f:RegisterEvent("PLAYER_MONEY")
	f:RegisterEvent("PLAYER_TRADE_MONEY")
	f:RegisterEvent("TRADE_MONEY_CHANGED")
	f:SetScript("OnEvent", function (self)
		self.gold:SetText(K.FormatMoney(GetMoney(), 12))
	end)

	do
		Token3:ClearAllPoints()
		Token3:SetPoint("TOP", f, "BOTTOM", -70, -10)
		Token2:ClearAllPoints()
		Token2:SetPoint("LEFT", Token3, "RIGHT", 14, 0)
		Token1:ClearAllPoints()
		Token1:SetPoint("LEFT", Token2, "RIGHT", 14, 0)
	end

	for i = 1, 3 do
		local Token = _G["BackpackTokenFrameToken" .. i]
		local Icon = _G["BackpackTokenFrameToken" .. i .. "Icon"]
		local Count = _G["BackpackTokenFrameToken" .. i .. "Count"]

		Token:SetParent(f)
		Token:SetScale(1)
		Token:CreateBackdrop()
		Token.Backdrop:SetOutside(Icon)
		Token.Backdrop:SetFrameLevel(6)
		Token:SetFrameStrata("MEDIUM")
		Token:SetFrameLevel(51)

		Icon:SetSize(16, 16)
		Icon:SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		Icon:SetPoint("LEFT", Token, "RIGHT", -8, 2)

		Count:SetFontObject(BAGS_FONT)
		Count:SetShadowOffset(0, 0)
	end

	local button = CreateFrame("Button", nil, f)
	button:EnableMouse(true)
	button:RegisterForClicks("LeftButtonUp", "RightButtonUp")
	button:SetAllPoints(detail)
	button.ttText = L["Inventory"].Right_Click_Search
	button:SetScript("OnClick", function(self, btn)
		if btn == "RightButton" then
			self:GetParent().detail:Hide()
			self:GetParent().gold:Hide()
			self:GetParent().editbox:Show()
			self:GetParent().editbox:HighlightText()
		else
			if self:GetParent().editbox:IsShown() then
				self:GetParent().editbox:Hide()
				self:GetParent().editbox:ClearFocus()
				self:GetParent().detail:Show()
				self:GetParent().gold:Show()
				Stuffing:SearchReset()
			end
		end
	end)

	button:SetScript("OnEnter", Stuffing_TooltipShow)
	button:SetScript("OnLeave", Stuffing_TooltipHide)

	f.bagsButton = CreateFrame("Button", nil, f)
	f.bagsButton:SetSize(16, 16)
	f.bagsButton:CreateBorder()
	f.bagsButton:SetPoint("TOPRIGHT", f, -32, -7)
	f.bagsButton:SetNormalTexture("Interface\\Buttons\\Button-Backpack-Up")
	f.bagsButton:GetNormalTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
	f.bagsButton:GetNormalTexture():SetAllPoints()
	f.bagsButton:SetPushedTexture("Interface\\Buttons\\Button-Backpack-Up")
	f.bagsButton:GetPushedTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
	f.bagsButton:GetPushedTexture():SetAllPoints()
	f.bagsButton:StyleButton(nil, true)
	f.bagsButton.ttText = L["Inventory"].Show_Bags
	f.bagsButton:SetScript("OnEnter", Stuffing_TooltipShow)
	f.bagsButton:SetScript("OnLeave", Stuffing_TooltipHide)
	f.bagsButton:SetScript("OnClick", function()
		PlaySound("igmainmenuoption")
		if bag_bars == 1 then
			bag_bars = 0
		else
			bag_bars = 1
		end

		if Stuffing.frame and Stuffing.frame:IsShown() then
			Stuffing:Layout()
		end
	end)

	f.sortButton = CreateFrame("Button", nil, f)
	f.sortButton:SetSize(16, 16)
	f.sortButton:CreateBorder()
	f.sortButton:StyleButton(nil, true)
	f.sortButton:SetPoint("TOPRIGHT", f.bagsButton, -22, 0)
	f.sortButton:SetNormalTexture("Interface\\ICONS\\INV_Pet_Broom")
	f.sortButton:GetNormalTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
	f.sortButton:GetNormalTexture():SetAllPoints()
	f.sortButton:SetPushedTexture("Interface\\ICONS\\INV_Pet_Broom")
	f.sortButton:GetPushedTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
	f.sortButton:GetPushedTexture():SetAllPoints()
	f.sortButton:SetDisabledTexture("Interface\\ICONS\\INV_Pet_Broom")
	f.sortButton:GetDisabledTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
	f.sortButton:GetDisabledTexture():SetAllPoints()
	f.sortButton:GetDisabledTexture():SetDesaturated(1)
	f.sortButton.ttText = _G.BAG_FILTER_CLEANUP
	f.sortButton:SetScript("OnEnter", Stuffing_TooltipShow)
	f.sortButton:SetScript("OnLeave", Stuffing_TooltipHide)
	f.sortButton:SetScript("OnMouseUp", function()
		f:UnregisterAllEvents() -- Unregister to prevent unnecessary updates
		f.registerUpdate = true -- Set variable that indicates this bag should be updated when sorting is done
		ModuleSort:CommandDecorator(ModuleSort.SortBags, "bags")()
	end)

	--Vendor Grays
	f.vendorGraysButton = CreateFrame('Button', nil, f)
	f.vendorGraysButton:SetSize(16, 16)
	f.vendorGraysButton:CreateBorder()
	f.vendorGraysButton:SetPoint("RIGHT", f.sortButton, "LEFT", -5, 0)
	f.vendorGraysButton:SetNormalTexture("Interface\\ICONS\\INV_Misc_Coin_01")
	f.vendorGraysButton:GetNormalTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
	f.vendorGraysButton:GetNormalTexture():SetAllPoints()
	f.vendorGraysButton:SetPushedTexture("Interface\\ICONS\\INV_Misc_Coin_01")
	f.vendorGraysButton:GetPushedTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
	f.vendorGraysButton:GetPushedTexture():SetAllPoints()
	f.vendorGraysButton:StyleButton(nil, true)
	f.vendorGraysButton.ttText = "Vendor / Delete Grays"
	f.vendorGraysButton:SetScript("OnEnter", Stuffing_TooltipShow)
	f.vendorGraysButton:SetScript("OnLeave", Stuffing_TooltipHide)
	f.vendorGraysButton:SetScript("OnClick", VendorGrayCheck)

	if K.Level >= 100 then
		-- Artifact Button
		f.ArtifactButton = CreateFrame("Button", nil, f, "BankItemButtonGenericTemplate")
		f.ArtifactButton:SetSize(16, 16)
		f.ArtifactButton:CreateBorder()
		f.ArtifactButton:SetPoint("RIGHT", f.vendorGraysButton, "LEFT", -5, 0)
		f.ArtifactButton:SetNormalTexture("Interface\\ICONS\\Achievement_doublejeopardy")
		f.ArtifactButton:GetNormalTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.ArtifactButton:GetNormalTexture():SetAllPoints()
		f.ArtifactButton:SetPushedTexture("Interface\\ICONS\\Achievement_doublejeopardy")
		f.ArtifactButton:GetPushedTexture():SetTexCoord(K.TexCoords[1], K.TexCoords[2], K.TexCoords[3], K.TexCoords[4])
		f.ArtifactButton:GetPushedTexture():SetAllPoints()
		f.ArtifactButton:StyleButton(nil, true)
		f.ArtifactButton:RegisterForClicks("RightButtonUp")
		f.ArtifactButton:SetScript("OnLeave", Stuffing_TooltipHide)
		f.ArtifactButton:SetScript("PreClick", function(self)
			for bag = 0, 4 do
				for slot = 1, GetContainerNumSlots(bag) do
					if IsArtifactPowerItem(GetContainerItemID(bag, slot)) then
						self:GetParent():SetID(bag)
						self:SetID(slot)
						return
					end
				end
			end
		end)

		f.ArtifactButton:SetScript("OnEnter", function(self)
			self:UpdateTooltip()
		end)

		f.ArtifactButton:HookScript("OnClick", function(self)
			if GameTooltip:IsForbidden() then
				return
			end

			if GameTooltip:GetOwner() == self then
				self:UpdateTooltip()
			end
		end)

		f.ArtifactButton.UpdateTooltip = function(self)
			if GameTooltip:IsForbidden() then
				return
			end

			local count = 0
			for bag = 0, 4 do
				for slot = 1, GetContainerNumSlots(bag) do
					if IsArtifactPowerItem(GetContainerItemID(bag, slot)) then
						count = count + 1
					end
				end
			end

			GameTooltip:SetOwner(self, "ANCHOR_LEFT")
			GameTooltip:AddLine(ARTIFACT_POWER..": "..count)
			GameTooltip:AddLine(L["Inventory"].Artifact_Use)
			GameTooltip:Show()
		end
	end

	if K.Level >= 100 then
		gold:SetPoint("RIGHT", f.ArtifactButton, "LEFT", -6, 0)
	else
		gold:SetPoint("RIGHT", f.vendorGraysButton, "LEFT", -6, 0)
	end

	f.editbox = editbox
	f.detail = detail
	f.button = button
	f.gold = gold

	self.frame = f

	f:Hide()
end

function Stuffing:Layout(isBank)
	local slots = 0
	local rows = 0
	local off = 20
	local cols, f, bs

	if isBank then
		bs = BAGS_BANK
		cols = C["Inventory"].BankColumns
		f = self.bankFrame
		self.bankFrame:SetAlpha(1)
	else
		bs = BAGS_BACKPACK
		cols = C["Inventory"].BagColumns
		f = self.frame

		f.editbox:SetFontObject(BAGS_FONT)

		f.detail:SetFontObject(BAGS_FONT)

		f.gold:SetText(K.FormatMoney(GetMoney(), 12))
		f.gold:SetFontObject(BAGS_FONT)

		f.detail:ClearAllPoints()
		f.detail:SetPoint("TOPLEFT", f, 12, -8)
		f.detail:SetPoint("RIGHT", f, -140, 0)
	end

	f:SetClampedToScreen(1)
	f:CreateBorder()

	local fb = f.bags_frame
	if bag_bars == 1 then
		fb:SetClampedToScreen(1)
		fb:CreateBorder()

		local bsize = C["Inventory"].ButtonSize
		local w = 2 * 8

		w = w + ((#bs - 1) * bsize)
		w = w + ((#bs - 2) * 6)

		fb:SetHeight(2 * 8 + bsize)
		fb:SetWidth(w)
		fb:Show()
	else
		fb:Hide()
	end

	local idx = 0
	for _, v in ipairs(bs) do
		if (not isBank and v <= 3) or (isBank and v ~= -1) then
			local bsize = C["Inventory"].ButtonSize
			local b = self:BagFrameSlotNew(fb, v)
			local xoff = 8

			xoff = xoff + (idx * bsize)
			xoff = xoff + (idx * 6)

			b.frame:ClearAllPoints()
			b.frame:SetPoint("LEFT", fb, "LEFT", xoff, 0)
			b.frame:SetSize(bsize, bsize)

			local btns = self.buttons
			b.frame:HookScript("OnEnter", function(self)
				local bag
				if isBank then
					bag = v
				else
					bag = v + 1
				end

				for _, val in ipairs(btns) do
					if val.bag == bag then
						val.frame:SetAlpha(1)
					else
						val.frame:SetAlpha(0.2)
					end
				end
			end)

			b.frame:HookScript("OnLeave", function(self)
				for _, btn in ipairs(btns) do
					btn.frame:SetAlpha(1)
				end
			end)

			b.frame:SetScript("OnClick", nil)

			idx = idx + 1
		end
	end

	for _, i in ipairs(bs) do
		local x = GetContainerNumSlots(i)
		if x > 0 then
			if not self.bags[i] then
				self.bags[i] = self:BagNew(i, f)
			end

			slots = slots + GetContainerNumSlots(i)
		end
	end

	rows = math_floor(slots / cols)
	if (slots % cols) ~= 0 then
		rows = rows + 1
	end

	f:SetWidth(cols * C["Inventory"].ButtonSize + (cols - 1) * C["Inventory"].ButtonSpace + 10 * 2)
	f:SetHeight(rows * C["Inventory"].ButtonSize + (rows - 1) * C["Inventory"].ButtonSpace + off + 10 * 2)

	local idx = 0
	for _, i in ipairs(bs) do
		local bag_cnt = GetContainerNumSlots(i)
		local specialType = select(2, GetContainerNumFreeSlots(i))
		if bag_cnt > 0 then
			self.bags[i] = self:BagNew(i, f)
			local bagType = self.bags[i].bagType

			self.bags[i]:Show()
			for j = 1, bag_cnt do
				local b, isnew = self:SlotNew(i, j)
				local xoff
				local yoff
				local x = (idx % cols)
				local y = math_floor(idx / cols)

				if isnew then
					table_insert(self.buttons, idx + 1, b)
				end

				xoff = 10 + (x * C["Inventory"].ButtonSize) + (x * C["Inventory"].ButtonSpace)
				yoff = off + 10 + (y * C["Inventory"].ButtonSize) + ((y) * C["Inventory"].ButtonSpace)
				yoff = yoff * -1

				b.frame:ClearAllPoints()
				b.frame:SetPoint("TOPLEFT", f, "TOPLEFT", xoff, yoff)
				b.frame:SetSize(C["Inventory"].ButtonSize, C["Inventory"].ButtonSize)
				b.frame.lock = false
				b.frame:SetAlpha(1)

				if bagType == ST_FISHBAG then
					b.frame:SetBackdropBorderColor(107/255, 150/255, 255/255) -- Tackle
					b.frame.lock = true
				elseif bagType == ST_SPECIAL then
					if specialType == 0x0008 then -- Leatherworking
						b.frame:SetBackdropBorderColor(224/255, 187/255, 74/255)
					elseif specialType == 0x0010 then -- Inscription
						b.frame:SetBackdropBorderColor(74/255, 77/255, 224/255)
					elseif specialType == 0x0020 then -- Herbs
						b.frame:SetBackdropBorderColor(18/255, 181/255, 32/255)
					elseif specialType == 0x0040 then -- Enchanting
						b.frame:SetBackdropBorderColor(194/255, 4/255, 204/255)
					elseif specialType == 0x0080 then -- Engineering
						b.frame:SetBackdropBorderColor(232/255, 118/255, 46/255)
					elseif specialType == 0x0200 then -- Gems
						b.frame:SetBackdropBorderColor(8/255, 180/255, 207/255)
					elseif specialType == 0x0400 then -- Mining
						b.frame:SetBackdropBorderColor(138/255, 103/255, 9/255)
					elseif specialType == 0x8000 then -- Fishing
						b.frame:SetBackdropBorderColor(107/255, 150/255, 255/255)
					elseif specialType == 0x10000 then -- Cooking
						b.frame:SetBackdropBorderColor(222/255, 13/255, 65/255)
					end
					b.frame.lock = true
				end

				self:SlotUpdate(b)
				idx = idx + 1
			end
		end
	end
end

function Stuffing:ADDON_LOADED(addon)
	if addon ~= "KkthnxUI" then
		return nil
	end

	self:RegisterEvent("BAG_UPDATE")
	self:RegisterEvent("CURRENCY_DISPLAY_UPDATE")
	self:RegisterEvent("ITEM_LOCK_CHANGED")
	--self:RegisterEvent("ITEM_UNLOCKED")
	self:RegisterEvent("BANKFRAME_OPENED")
	self:RegisterEvent("BANKFRAME_CLOSED")
	self:RegisterEvent("GUILDBANKFRAME_OPENED")
	self:RegisterEvent("GUILDBANKFRAME_CLOSED")
	self:RegisterEvent("PLAYERBANKSLOTS_CHANGED")
	self:RegisterEvent("BAG_CLOSED")
	self:RegisterEvent("BAG_UPDATE_COOLDOWN")
	-- self:RegisterEvent("SCRAPPING_MACHINE_SHOW")

	self:InitBags()

	table_insert(UISpecialFrames, "StuffingFrameBags")
	table_insert(UISpecialFrames, "StuffingFrameReagent")

	ToggleBackpack = Stuffing_Toggle
	ToggleBag = Stuffing_Toggle
	ToggleAllBags = Stuffing_Toggle
	OpenAllBags = Stuffing_Open
	OpenBackpack = Stuffing_Open
	CloseAllBags = Stuffing_Close
	CloseBackpack = Stuffing_Close

	BankFrame:UnregisterAllEvents()
	BankFrame:SetScale(0.0001)
	BankFrame:SetAlpha(0)
	BankFrame:SetPoint("TOPLEFT")

	function ManageBackpackTokenFrame() end

	if ContainerFrame5 then
		ContainerFrame5:EnableMouse(false)
	end
end

function Stuffing:PLAYER_ENTERING_WORLD()
	Stuffing:UnregisterEvent("PLAYER_ENTERING_WORLD")

	ToggleBackpack()
	ToggleBackpack()
end

function Stuffing:PLAYERBANKSLOTS_CHANGED(id)
	if id > 28 then
		for _, v in ipairs(self.bagframe_buttons) do
			if v.frame and v.frame.GetInventorySlot then
				BankFrameItemButton_Update(v.frame)
				BankFrameItemButton_UpdateLocked(v.frame)

				if not v.frame.tooltipText then
					v.frame.tooltipText = ""
				end
			end
		end
	end

	if self.bankFrame and self.bankFrame:IsShown() then
		self:BagSlotUpdate(-1)
	end
end

function Stuffing:CURRENCY_DISPLAY_UPDATE()
	BackpackTokenFrame_Update()
end

function Stuffing:BAG_UPDATE(id)
	self:BagSlotUpdate(id)
end

function Stuffing:ITEM_LOCK_CHANGED(bag, slot)
	if slot == nil then
		return
	end

	for _, v in ipairs(self.buttons) do
		if v.bag == bag and v.slot == slot then
			self:SlotUpdate(v)
			break
		end
	end
end

function Stuffing:ITEM_UNLOCKED(bag, slot)
	if slot == nil then
		return
	end

	for _, v in ipairs(self.buttons) do
		if v.bag == bag and v.slot == slot then
			self:SlotUpdate(v)
			break
		end
	end
end

function Stuffing:BANKFRAME_OPENED()
	if not self.bankFrame then
		self:InitBank()
	end

	self:Layout(true)
	for _, x in ipairs(BAGS_BANK) do
		self:BagSlotUpdate(x)
	end

	self.bankFrame:Show()
	Stuffing_Open()
end

function Stuffing:BANKFRAME_CLOSED()
	if StuffingFrameReagent then
		StuffingFrameReagent:Hide()
	end

	if self.bankFrame then
		self.bankFrame:Hide()
	end
end

function Stuffing:GUILDBANKFRAME_OPENED()
	Stuffing_Open()
end

function Stuffing:GUILDBANKFRAME_CLOSED()
	Stuffing_Close()
end

function Stuffing:BAG_CLOSED(id)
	local b = self.bags[id]
	if b then
		table_remove(self.bags, id)
		b:Hide()
		table_insert(trashBag, #trashBag + 1, b)
	end

	while true do
		local changed = false

		for i, v in ipairs(self.buttons) do
			if v.bag == id then
				v.frame:Hide()
				v.frame.lock = false

				table_insert(trashButton, #trashButton + 1, v.frame)
				table_remove(self.buttons, i)

				v = nil
				changed = true
			end
		end

		if not changed then
			break
		end
	end

	Stuffing_Close()
end

function Stuffing:BAG_UPDATE_COOLDOWN()
	for _, v in pairs(self.buttons) do
		--self:UpdateCooldowns(v)
	end
end

function Stuffing:SCRAPPING_MACHINE_SHOW()
	for i = 0, #BAGS_BACKPACK - 1 do
		Stuffing:BAG_UPDATE(i)
	end
end

-- Kill Blizzard functions
LootWonAlertFrame_OnClick = K.Noop
LootUpgradeFrame_OnClick = K.Noop
StorePurchaseAlertFrame_OnClick = K.Noop
LegendaryItemAlertFrame_OnClick = K.Noop