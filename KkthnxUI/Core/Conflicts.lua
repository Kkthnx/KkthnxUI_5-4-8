local K, C = unpack(select(2, ...))

-- Prevent users config errors and using other UIs over mine.

local _G = _G

local DisableAddOn = _G.DisableAddOn
local ReloadUI = _G.ReloadUI
local IsAddOnLoaded = _G.IsAddOnLoaded

-- Force user to disable KkthnxUI if another AddOn is enabled with it!
if IsAddOnLoaded("KkthnxUI") and IsAddOnLoaded("Tukui") or IsAddOnLoaded("ElvUI") or IsAddOnLoaded("DiabolicUI") or IsAddOnLoaded("DuffedUI") or IsAddOnLoaded("ShestakUI") then
	StaticPopupDialogs.KKTHNXUI_INCOMPATIBLE = {
		text = "Oh no, you have |cff4488ffKkthnxUI|r and another UserInterface enabled at the same time. Disable KkthnxUI!",
		button1 = "Disable KkthnxUI",
		OnAccept = function()
			DisableAddOn("KkthnxUI")
			ReloadUI()
		end,
		OnCancel = function()
			DisableAddOn("KkthnxUI")
			ReloadUI()
		end,
		timeout = 0,
		whileDead = 1,
		hideOnEscape = 0,
		showAlert = 1,
		EditBoxOnEscapePressed = function()
			DisableAddOn("KkthnxUI")
			ReloadUI()
		end,
	}

	StaticPopup_Show("KKTHNXUI_INCOMPATIBLE")
	return
end

-- Actionbar Conflicts
if C["ActionBar"].RightBars > 3 then
	C["ActionBar"].RightBars = 3
end

if C["ActionBar"].BottomBars > 3 then
	C["ActionBar"].BottomBars = 3
end

if C["ActionBar"].BottomBars == 3 and C["ActionBar"].RightBars == 3 then
	C["ActionBar"].BottomBars = 3
	C["ActionBar"].RightBars = 2
end

if C["ActionBar"].SplitBars == true then
	C["ActionBar"].BottomBars = 3
	C["ActionBar"].RightBars = 2
end

if C["ActionBar"].BottomBars < 1 then
	C["ActionBar"].BottomBars = 1
end

if C["ActionBar"].PetBarHorizontal == true then
	C["ActionBar"].StanceBarHorizontal = false
end

-- Auto-overwrite script config is X addon is found. Here we use our own functions to check for addons.
if C["DataBars"].Enable == false then
	C["DataBars"].TrackHonor = false
end

if IsAddOnLoaded("flyPlateBuffs") then
	C["Nameplates"].TrackAuras = false
end

if IsAddOnLoaded("SexyMap")
or IsAddOnLoaded("bdMinimap")
or IsAddOnLoaded("BasicMinimap")
or IsAddOnLoaded("RicoMiniMap")
or IsAddOnLoaded("Chinchilla") then
	C["Minimap"].Enable = false
end

if IsAddOnLoaded("XPerl")
or IsAddOnLoaded("Stuf")
or IsAddOnLoaded("PitBull4")
or IsAddOnLoaded("ShadowedUnitFrames") then
	C["Unitframe"].Enable = false
	C["Party"].Enable = false
	C["Arena"].Enable = false
	C["Boss"].Enable = false
end

if IsAddOnLoaded("Dominos")
or IsAddOnLoaded("Bartender4")
or IsAddOnLoaded("RazerNaga")
or IsAddOnLoaded("daftMainBar")
or (IsAddOnLoaded("ConsolePortBar") and IsAddOnLoaded("ConsolePort")) then -- We have to check for main ConsolePort addon too.
	C["ActionBar"].Enable = false
end

if IsAddOnLoaded("WorldQuestTracker")
or IsAddOnLoaded("Mapster")
or IsAddOnLoaded("WorldQuestsList") then
	C["WorldMap"].SmallWorldMap = false
end

if IsAddOnLoaded("AdiBags")
or IsAddOnLoaded("ArkInventory")
or IsAddOnLoaded("cargBags_Nivaya")
or IsAddOnLoaded("cargBags")
or IsAddOnLoaded("Bagnon")
or IsAddOnLoaded("Combuctor")
or IsAddOnLoaded("TBag")
or IsAddOnLoaded("BaudBag") then
	C["Inventory"].Enable = false
end

if IsAddOnLoaded("Prat-3.0")
or IsAddOnLoaded("Chatter") then
	C["Chat"].Enable = false
end

if IsAddOnLoaded("TidyPlates")
or IsAddOnLoaded("Aloft")
or IsAddOnLoaded("Kui_Nameplates")
or IsAddOnLoaded("bdNameplates")
or IsAddOnLoaded("NiceNameplates") then
	C["Nameplates"].Enable = false
end

if IsAddOnLoaded("TipTop")
or IsAddOnLoaded("TipTac")
or IsAddOnLoaded("FreebTip")
or IsAddOnLoaded("bTooltip")
or IsAddOnLoaded("PhoenixTooltip")
or IsAddOnLoaded("Icetip")
or IsAddOnLoaded("rTooltip") then
	C["Tooltip"].Enable = false
end

if IsAddOnLoaded("TipTacTalents") then
	C["Tooltip"].Talents = false
end

if IsAddOnLoaded("ConsolePortBar") then
	C["DataBars"].Enable = false
end

if IsAddOnLoaded("cInterrupt") then
	C["Announcements"].Interrupt = false
end

if IsAddOnLoaded("NiceBubbles") then
	C["Skins"].ChatBubble = false
end

if IsAddOnLoaded("ChatSounds") then
	C["Chat"].WhispSound = false
end

if IsAddOnLoaded("MBB")
or IsAddOnLoaded("MinimapButtonFrame") then
	C["Minimap"].CollectButtons = false
end

if IsAddOnLoaded("OmniCC")
or IsAddOnLoaded("ncCooldown")
or IsAddOnLoaded("CooldownCount") then
	C["ActionBar"].Cooldowns = false
end